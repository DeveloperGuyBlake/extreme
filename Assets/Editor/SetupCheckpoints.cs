﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SetupCheckpoints : MonoBehaviour
{
    //This script was written by Blake using Steam Deck. Neat!

    [MenuItem("Extreme/Fill Checkpoint Data")]
    static void FillCheckpointData()
    {
        Checkpoint[] cps;
        EventManager em = GameObject.Find("ManagerContainer").GetComponent<EventManager>();
        cps = em.checkpoints;


        string message = cps.Length.ToString() + " checkpoints are listed in EventManager. Is this correct?";
        if (EditorUtility.DisplayDialog("Fill Checkpoint Data",message,"Yes, fill data","Cancel"))
        {
            for (int i = 0; i < cps.Length; i++)
            {
                if (i!=cps.Length-1) cps[i].nextCheckpoint = cps[i+1];
                else if (i==cps.Length-1) cps[i].nextCheckpoint = cps[0];
                EditorUtility.SetDirty(cps[i]);
            }
            Debug.Log("Done");
        }
    }
}
