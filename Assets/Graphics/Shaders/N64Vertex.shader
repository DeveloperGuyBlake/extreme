﻿//Shader written by Unity forum users at https://forum.unity.com/threads/shader-ideas-for-replicating-n64-bi-linear-filtering.936119/
Shader "N64_Vertex"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB)", 2D) = "white" {}
    [Range(0,1)]
        _Cutoff("Alpha Cutoff", Range(0,1)) = 0.5
    [Range(0,1)]
        _Glossiness("Smoothness", Range(0,1)) = 0.5
    [Range(0,1)]
        _Metallic("Metallic", Range(0,1)) = 0.0
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200
        CGPROGRAM
        #pragma surface surf Standard alphatest:_Cutoff fullforwardshadows
        sampler2D _MainTex;
        float4 _MainTex_TexelSize;
        struct Input
        {
            float2 uv_MainTex;
            float4 vertexColor : COLOR;
        };
        struct v2f {
            float4 pos : SV_POSITION;
            fixed4 color : COLOR;
        };
        fixed4 N64Filtering(sampler2D tex, float2 uv, float4 scale)
        {
            //texel coords
            float2 texel = uv * scale.zw;
            //get mip map coords and scaling
            float2 mipX = ddx(texel), mipY = ddy(texel);
            float delta_max_sqr = max(dot(mipX, mipX), dot(mipY, mipY));
            float mip = max(0.0, 0.5 * log2(delta_max_sqr));
            
            float size = pow(2, floor(mip));
            scale.xy *= size;
            scale.zw /= size;
            texel = texel / size - 0.5;
            //sample points
            float2 fracTexl = frac(texel);
            float2 uv1 = (floor(texel + fracTexl.yx) + 0.5) * scale.xy;
            fixed4 out1 = tex2Dlod(tex, float4(uv1, 0, mip));
            float2 uv2 = (floor(texel) + float2(1.5, 0.5)) * scale.xy;
            fixed4 out2 = tex2Dlod(tex, float4(uv2, 0, mip));
            float2 uv3 = (floor(texel) + float2(0.5, 1.5)) * scale.xy;
            fixed4 out3 = tex2Dlod(tex, float4(uv3, 0, mip));
            //calculate blend and apply
            float3 blend = float3(abs(fracTexl.x + fracTexl.y - 1), min(abs(fracTexl.xx - float2(0, 1)), abs(fracTexl.yy - float2(1, 0))));
            float4 _outTex = out1 * blend.x + out2 * blend.y + out3 * blend.z;
            // blend and return
            return _outTex;
        }
        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = N64Filtering(_MainTex, IN.uv_MainTex, _MainTex_TexelSize) * _Color * IN.vertexColor;;
            o.Albedo = c.rgb * IN.vertexColor; // Combine normal color with the vertex color
            // Metallic and smoothness come from slider variables
                o.Metallic = _Metallic;
                o.Smoothness = _Glossiness;
                o.Alpha = c.a;
        }
        ENDCG
    }
        FallBack "Diffuse"
}
    
