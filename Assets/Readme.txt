Project San Francisco Alpha 0.7.0 by DevGuyBlake and Stevie. Content based on the works of Midway/Atari. This game uses modified assets from the Original San Francisco Rush console games.
Thanks to Ace9921, SaltyChicken, Michael F9000, and Weg for additional support.
Special thanks to Barry Leitch and Stephen Riesenberger

                     _____  _____   ____       _ ______ _____ _______                      
                    |  __ \|  __ \ / __ \     | |  ____/ ____|__   __|                     
                    | |__) | |__) | |  | |    | | |__ | |       | |                        
                    |  ___/|  _  /| |  | |_   | |  __|| |       | |                        
                    | |    | | \ \| |__| | |__| | |___| |____   | |                        
   _____         _  |_|    |_|__\_\\____/ \____/|______\_____|__|_|___  _____  _____ ____  
  / ____|  /\   | \ | |    |  ____|  __ \     /\   | \ | |/ ____|_   _|/ ____|/ ____/ __ \ 
 | (___   /  \  |  \| |    | |__  | |__) |   /  \  |  \| | |      | | | (___ | |   | |  | |
  \___ \ / /\ \ | . ` |    |  __| |  _  /   / /\ \ | . ` | |      | |  \___ \| |   | |  | |
  ____) / ____ \| |\  |    | |    | | \ \  / ____ \| |\  | |____ _| |_ ____) | |___| |__| |
 |_____/_/    \_\_| \_|    |_|    |_|  \_\/_/    \_\_| \_|\_____|_____|_____/ \_____\____/ 



GAME CONTROLS:

(Manual transmissions only, currently.)

Keyboard:
-Up Arrow or W          - Accelerate
-Down Arrow or S        - Brake
-N                      - Shift Up
-Manual                 - Shift Down
-Space (while airborne) - Wings  

Gamepads:
-Right Trigger          - Gas
-Left Trigger           - Brake
-A                      - Shift Up
-X                      - Shift Down
-B (while airborne)     - Wings
-Left Bumper            - Look Back
-Right Stick            - Camera Look

To reverse: Shift down into reverse and accelerate.

-Keyboard P             - Cheat: Self Destruct (Temporary. Intended as a placeholder until Abort is implemented.)
-Keyboard Alt+Enter     - Toggle full screen

-----

CHANGELOG:
0.7.0 Changelog:
-Added Stunt mode.
-Added "Classic" stunt map from Rush 2. This map can only be played in Stunt mode.
--Race maps are currently not playable in Stunt mode, but this is planned to be changed in the future.
-Added Stunt mode point scoring system based on Rush 2049's scoring system, but with a few tweaks.
-Improved The Rock and Crash tracks with atmospheric lighting, environmental audio, and new animated objects. Expect a much more immersive environment.
-Removed 'Doin Tyme' music track from The Rock as it doesn't fit with The Rock's newly-improved theme. It will be re-introduced when The Rock's daytime variant is added.
-Respawning cars now spawn with forward momentum in race modes.
-Respawning cars now spawn as non-solid and have a blinking animation.
-All cars are granted invincibility in Stunt mode.
-Added car rotational velocity check for explosion trigger; Crashes are more dramatic and cars are less likely to explode if they are performing stunts or rolling. Cars are less likely to explode in Stunt mode than in Practice or Single Race modes.
-Fixed desync of voice and UI on race start countdown when spawning a large number of drones.
-Fixed typos in certain splash messages.

-----

KNOWN ISSUES:

-Drones sometimes get stuck. (Legacy)
-Landing a car on the rear wheels will cause it to bounce violently into the air. (Legacy)
-Vehicles sometimes clip through geometry during very fast collisions. (Legacy)
-Vehicle engine audio is broken while the car is in reverse. (Legacy)
-Drone pathfinding is still in development. Drones may crash often or behave erratically.
-Displayed race result is sometimes incorrect due to drones finishing races twice and occupying 2 result slots.
-Camera behavior is unfinished.
-Application now pauses when in the background.
-Drones enable their lights during the day.
-Drones' brakelights don't emit when they are braking.
-The menu and user interface will not display correctly at resolutions much below 1920x1080.



-----

ADDITIONAL NOTES:

-There is no Tachometer representing engine values yet. Listen to the sound of the car's engine and time your shifts accordingly. Practice mode is recommended to get familiar with the timing, but ratios are similar to the original Rush games as well.
-Playstation and Generic gamepads are untested but should work. First-party Xbox 360 and Xbox One/SeriesX controllers are recommended.
-1920x1080 resolution is recommended as it has the most testing but other screen resolutions should still work. Please report any issues.
-Submit your best times to #submissions on the Team Rush Discord Server!