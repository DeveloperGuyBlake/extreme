﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamAnimSequence : MonoBehaviour
{
    SmoothFollow smoothFollow;
    public CarController cc;
    GameObject camPoint1;
    GameObject camPoint2;
    GameObject camPoint3;
    GameObject camPoint4;
    Vector3 lerpPos;
    int playerNumber;
    bool introRunning = true;

    void Awake()
    {
        smoothFollow = GetComponent<SmoothFollow>();
        smoothFollow.lookOnly = true;
    }

    void Start()
    {
        playerNumber = cc.playerID;

        if (playerNumber == 1)
        {
            camPoint1 = EventManager.eventManager.cam1point1;
            camPoint2 = EventManager.eventManager.cam1point2;
            camPoint3 = EventManager.eventManager.cam1point3;
            camPoint4 = EventManager.eventManager.cam1point4;
        }

        this.gameObject.transform.position = camPoint1.transform.position;
    }

    void Update()
    {
        if (introRunning)
        {
            float t = (Time.time);//TODO: GENERATE A CURVE FROM 4 POINTS AND FOLLOW IT AS A PATH RATHER THAN CHASE EACH POINT
            if (t < 0.7f) lerpPos = Vector3.Lerp(transform.position, camPoint2.transform.position, t * 3 * Time.deltaTime);
            else if (t >= 0.7f && t < 1.2f) lerpPos = Vector3.Lerp(transform.position, camPoint3.transform.position, t * 3 * Time.deltaTime);
            else if (t >= 1.2f && t < 2.3f) lerpPos = Vector3.Lerp(transform.position, camPoint4.transform.position, t * 3 * Time.deltaTime);
            else if (t >= 2.3f)//TODO: CAN'T CHANGE CAMERA (in CarController) UNTIL INTRORUNNING IS FALSE
            {
                introRunning = false;
                smoothFollow.lookOnly = false;
                enabled = false;
            }
            transform.position = lerpPos;
        }

    }
}
