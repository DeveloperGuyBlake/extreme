﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SmoothFollow : MonoBehaviour {
    
	// The target we are following
	public Transform target;
	// The distance in the x-z plane to the target
	public float distance = 10.0f;
	// the height we want the camera to be above the target
	public float height = 5.0f;
	//
	public float heightDamping = 2.0f;
	public float rotationDamping = 3.0f;
	//[HideInInspector]
	public bool customEnabledBool = true;
	public bool lookOnly = false;
	public bool dontLook = false;

	public GameObject pausePanel;
	public GameObject raceResultUiObject;
	public Text finishPositionText;

	public GameObject glassSprite;
	public float lastBreakGlassTime;//the last time the camera glass was broken to prevent it from occurring too often

	[Header("Pause Menu")]

	public Animator resumeAnim;
	public Animator optionsAnim;
	public Animator controlsAnim;
	public Animator audioAnim;
	public Animator restartAnim;
	public Animator quitAnim;
	public AudioSource menuSource;
	public AudioClip cursorClip;

	int pointerVal;//the menu item that is currently highlighted from top to bottom.
	bool paused = false;
	bool hasMoved = false;

	void Awake()
	{
		pausePanel.SetActive(false);
	}

	void Start()
	{
		transform.SetParent(null);
	}

	void Update()//Update is essentially just a modified version of SmoothFollow.cs
	{
		if (!customEnabledBool) return;
		// Early out if we don't have a target
		if (!target) return;

		// Calculate the current rotation angles
		float wantedRotationAngle = target.eulerAngles.y;
		float wantedHeight = target.position.y + height;

		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;

		// Damp the rotation around the y-axis
		if (!lookOnly) currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
	
		// Damp the height
		if (!lookOnly) currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

		// Convert the angle into a rotation
		var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
	
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		if (!lookOnly) transform.position = target.position;
		if (!lookOnly) transform.position -= currentRotation * Vector3.forward * distance;

		// Set the height of the camera
		if (!lookOnly) transform.position = new Vector3(transform.position.x,currentHeight,transform.position.z);
	
		// Always look at the target
		if (!dontLook) transform.LookAt(target);
		else transform.rotation = Quaternion.Euler(90,0,0);


		/////PAUSE MENU NAVIGATION/////
		//Up
		if (paused && !hasMoved && Input.GetAxis("MenuVertical")>0.5f)
		{
			if (pointerVal == 1)
			{
				pointerVal = 6;
				hasMoved = true;
				resumeAnim.SetTrigger("Idle");
				quitAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 6)
			{
				pointerVal = 5;
				hasMoved = true;
				quitAnim.SetTrigger("Idle");
				restartAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 5)
			{
				pointerVal = 4;
				hasMoved = true;
				restartAnim.SetTrigger("Idle");
				audioAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 4)
			{
				pointerVal = 3;
				hasMoved = true;
				audioAnim.SetTrigger("Idle");
				controlsAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 3)
			{
				pointerVal = 2;
				hasMoved = true;
				controlsAnim.SetTrigger("Idle");
				optionsAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 2)
			{
				pointerVal = 1;
				hasMoved = true;
				optionsAnim.SetTrigger("Idle");
				resumeAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
		}
		//down
		else if (paused && !hasMoved && Input.GetAxis("MenuVertical")<-0.5f)
		{
			if (pointerVal == 1)
			{
				pointerVal = 2;
				hasMoved = true;
				resumeAnim.SetTrigger("Idle");
				optionsAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 2)
			{
				pointerVal = 3;
				hasMoved = true;
				optionsAnim.SetTrigger("Idle");
				controlsAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 3)
			{
				pointerVal = 4;
				hasMoved = true;
				controlsAnim.SetTrigger("Idle");
				audioAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 4)
			{
				pointerVal = 5;
				hasMoved = true;
				audioAnim.SetTrigger("Idle");
				restartAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 5)
			{
				pointerVal = 6;
				hasMoved = true;
				restartAnim.SetTrigger("Idle");
				quitAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
			else if (pointerVal == 6)
			{
				pointerVal = 1;
				hasMoved = true;
				quitAnim.SetTrigger("Idle");
				resumeAnim.SetTrigger("Highlight");
				menuSource.clip = cursorClip;
				menuSource.Play();
			}
		}
		//reset movement when release input
        if (hasMoved && Input.GetAxis("MenuVertical")<0.3f && Input.GetAxis("MenuVertical")>-0.3f && Input.GetAxis("MenuHorizontal")<0.3f && Input.GetAxis("MenuHorizontal")>-0.3f) hasMoved = false;

		if (paused && Input.GetButtonDown("Submit1"))
		{
			if (pointerVal == 1)
			EventManager.eventManager.TogglePause();
		}
	}

	public void Pause(bool pausing)
	{
		if (pausing)
		{
			paused = true;
			pausePanel.SetActive(true);
			pointerVal = 1;
			resumeAnim.SetTrigger("Highlight");
		}
		else
		{
			paused = false;
			resumeAnim.SetTrigger("Idle");
			optionsAnim.SetTrigger("Idle");
			controlsAnim.SetTrigger("Idle");
			audioAnim.SetTrigger("Idle");
			restartAnim.SetTrigger("Idle");
			quitAnim.SetTrigger("Idle");
			pausePanel.SetActive(false);
		}
	}

	public void FinishLineDisplay(int finishPos, bool lastPlace)
	{
		if (lastPlace) finishPositionText.text = "LAST PLACE";
		else if (!lastPlace)
		{
			if (finishPos == 1) finishPositionText.text = "FIRST PLACE!";
			else if (finishPos == 2) finishPositionText.text = "SECOND PLACE!";
			else if (finishPos == 3) finishPositionText.text = "THIRD PLACE!";//TODO: Text color gold, silver, bronze. Else, white.
			else if (finishPos == 4) finishPositionText.text = "4th PLACE";
			else if (finishPos == 5) finishPositionText.text = "5th PLACE";
			else if (finishPos == 6) finishPositionText.text = "6th PLACE";
			else if (finishPos == 7) finishPositionText.text = "7th PLACE";
			else if (finishPos == 8) finishPositionText.text = "8th PLACE";
			else if (finishPos == 9) finishPositionText.text = "9th PLACE";
			else if (finishPos == 10) finishPositionText.text = "10th PLACE";
			else if (finishPos == 11) finishPositionText.text = "11th PLACE";
			else if (finishPos == 12) finishPositionText.text = "12th PLACE";
			else if (finishPos == 13) finishPositionText.text = "13th PLACE";
			else if (finishPos == 14) finishPositionText.text = "14th PLACE";
			else if (finishPos == 15) finishPositionText.text = "15th PLACE";
		}
		else 
		{
			Debug.LogError("FinishLineDisplay() invalid finishing position");
			finishPositionText.text = "errorNullPos";
		}
		raceResultUiObject.SetActive(true);
		//TODO: DISPLAY FASTEST LAP
	}

	public void BreakGlass(bool b)
	{
		glassSprite.SetActive(b);
	}

	public void setBreakTime()
	{
		int i = Random.Range(1,5);
		if (i == 1) glassSprite.transform.localScale = new Vector3(1,1,1);
		else if (i == 2) glassSprite.transform.localScale = new Vector3(-1,1,1);
		else if (i == 3) glassSprite.transform.localScale = new Vector3(-1,-1,1);
		else if (i == 4) glassSprite.transform.localScale = new Vector3(1,-1,1);
		lastBreakGlassTime = Time.time;
	}
}