﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrushTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("CRUSH TRIGGER");
        CarController c = other.gameObject.GetComponentInParent<CarController>();
        if (c != null) c.Explode(true);
    }
}
