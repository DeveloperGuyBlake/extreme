﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObjects : MonoBehaviour
{
    public GameObject[] activateObjects;
    public GameObject[] deactivateObjects;

    void OnTriggerEnter(Collider hit)
    {
        if (hit.GetComponentInParent<InputManager>())
        {
            //Debug.Log("playerTrigger");
            foreach (GameObject x in activateObjects) x.SetActive(true);
            foreach (GameObject y in deactivateObjects) y.SetActive(false);
        }
    }
}
