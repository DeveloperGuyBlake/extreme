﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script was used for blinking lights in PJSF

public class TimedTextureSwap : MonoBehaviour
{
    public Material mat;
    public float swapTime;
    public Texture alb1;
    public Texture emit1;
    public Texture alb2;
    public Texture emit2;
    float lastSwapTime = 0;
    bool swapped = false;

    void Update()
    {
        if (Time.time >= lastSwapTime + swapTime)
        {
            if (swapped)
            {
                mat.SetTexture("_MainTex",alb1);
                mat.SetTexture("_EmissionMap", emit1);
                swapped = false;
            }
            else
            {
                mat.SetTexture("_MainTex",alb2);
                mat.SetTexture("_EmissionMap", emit2);
                swapped = true;
            }
            lastSwapTime = Time.time;
        }
    }
}
