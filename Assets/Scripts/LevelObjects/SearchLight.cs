﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchLight : MonoBehaviour
{
    Transform playerCar;
    Light l;

    void Awake()
    {
        playerCar = EventManager.eventManager.carControllers[0].gameObject.transform;
        l = GetComponent<Light>();
    }

    void Update()
    {
        this.transform.LookAt(playerCar);
        //float dist = Vector3.Distance(playerCar.position, transform.position);
        //l.spotAngle = dist * 0.1f;
        //Debug.Log(dist + " | " + (dist * 0.1f));
    }
}
