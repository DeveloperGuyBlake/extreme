﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        Debug.LogError("KILL TRIGGER");
        CarController c = other.gameObject.GetComponentInParent<CarController>();
        if (c != null) c.Explode(true);
    }
}
