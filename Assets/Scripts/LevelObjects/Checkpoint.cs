﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [Tooltip("Finish line should be 1. Every checkpoint in sequence in the track's forward direction increments up by 1.\nRoutes that include an off-track spawn area such as GoldenGate and Presidio increment backward from the point they merge with the track, and values below 0 are allowed. Certain tracks of this design will result with multiple checkpoints with a value of 0; this is okay.")]
    public int checkpointID = -100;
    [Tooltip("Whether or not this checkpoint should add bonus time.")]
    public bool bonusTime = false;
    [Tooltip("Whether or not to use overridePos as a spawnpoint.")]
    public bool overrideSpawnPos = false;
    [Tooltip("If overrideSpawnPos is set to true, this transform will be used to place cars instead of this gameobject's transform.\nThis is usually used for shortcuts.\nEnsure that alternate route checkpoints share the same checkpoint value as their associated point on the main route, and that there are the same number of checkpoints on the main route as in the shortcut.")]
    public Transform overridePos;
    [Tooltip("The transform target that drones will chase after hitting this checkpoint(Y value will be ignored).\n Should be beyond the next checkpoint's trigger zone.")]//TODO: IGNORE TARGET TRANSFORM'S Y VALUE, ADD DRONES HITTING BRAKES TO AVOID LEAVING ROAD WIDTH
    public Transform droneTarget;
    [Tooltip("The speed in meters/second drones will slow to after passing through this checkpoint before accelerating again. Must be a positive value.\nSet to 0 for no limit (max acceleration).\nA high value like 1000 will cause the drone to be less than targetSpeed and instantly disable its speedLimit.")]
    public float droneSpeedLimit = 0;
    public Checkpoint nextCheckpoint;

    

    void Awake()
    {
        if (checkpointID == -100) Debug.LogError(this.gameObject.name + " has not been assigned a valid checkpoint ID value.");
        if (droneTarget == null) Debug.LogError(this.gameObject.name + " has no assigned droneTarget!");
        if (nextCheckpoint == null) Debug.LogError(this.gameObject.name + " has no assigned nextCheckpoint!");
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.layer != 9) Debug.LogError("Something other than a CarToEnv collider has triggered " + this.gameObject.name + "!");
        else
        {
            bool isDrone = false;
            CarController car = hit.GetComponentInParent<CarController>();
            if (car.isAI) isDrone = true;
            EventManager.eventManager.Checkpoint(car.playerID, checkpointID, bonusTime, isDrone);

            if (isDrone)
            {
                DroneScript ds;
                if (checkpointID == EventManager.eventManager.playerCPs[car.playerID-1])//checks to see if this checkpoint is the next one the drone should hit, so as to not assign incorrect targets when hitting wrong order checkpoints.
                {
                    //Debug.Log(this.gameObject.name + " is player " + car.playerID + "'s next checkpoint");
                    ds = hit.GetComponentInParent<DroneScript>();
                    ds.chaseTarget = droneTarget;
                }
                if (droneSpeedLimit != 0)
                {
                    ds = hit.GetComponentInParent<DroneScript>();
                    ds.targetSpeedMet = false;
                    ds.targetSpeed = droneSpeedLimit;
                    //Debug.Log("Set drone speed limit to " + droneSpeedLimit);
                }
            }
        }
    }
}
