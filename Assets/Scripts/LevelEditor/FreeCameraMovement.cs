﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeCameraMovement : MonoBehaviour
{
    Transform camTransform;
    float camMoveSpeed = 15;
    float camLookSensitivity = 30;
    float rotX;
    float rotY;

    bool canLook = false;//controls whether moving the mouse will cause the camera to rotate (Are we holding right mouse button down?)

    void Start()
    {
        camTransform = this.transform;
        Vector3 rot = camTransform.localRotation.eulerAngles;
        rotX = rot.x;
        rotY = rot.y;
    }

    void Update()
    {
        //////////WASDQE CAM MOVEMENT//////////
        if (Input.GetKeyDown(KeyCode.LeftShift)) camMoveSpeed = 30;//Hold shift to move camera faster
        else if (Input.GetKeyUp(KeyCode.LeftShift)) camMoveSpeed = 15;

        if (Input.GetKey(KeyCode.W))
        {
            camTransform.Translate((Vector3.forward * camMoveSpeed) * Time.deltaTime, Space.Self);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            camTransform.Translate((-Vector3.forward * camMoveSpeed) * Time.deltaTime, Space.Self);
        }

        if (Input.GetKey(KeyCode.D))
        {
            camTransform.Translate((Vector3.right * camMoveSpeed) * Time.deltaTime, Space.Self);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            camTransform.Translate((-Vector3.right * camMoveSpeed) * Time.deltaTime, Space.Self);
        }

        if (Input.GetKey(KeyCode.E))
        {
            camTransform.Translate((Vector3.up * camMoveSpeed) * Time.deltaTime, Space.Self);
        }
        else if (Input.GetKey(KeyCode.Q))
        {
            camTransform.Translate((-Vector3.up * camMoveSpeed) * Time.deltaTime, Space.Self);
        }

        //////////RIGHT-CLICK CAM LOOK//////////
        if (Input.GetKeyDown(KeyCode.Mouse1)) canLook = true;
        else if (Input.GetKeyUp(KeyCode.Mouse1)) canLook = false;

        if (canLook)
        {
            float mouseX = Input.GetAxis("EditorMouseX");
            float mouseY = Input.GetAxis("EditorMouseY");
            rotY += mouseX * camLookSensitivity * Time.deltaTime;
            rotX += mouseY * camLookSensitivity * Time.deltaTime;
            Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0);
            camTransform.rotation = localRotation;
        }
    }
}
