﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSource : MonoBehaviour
{
    public bool isMenu = false;
    public AudioClip[] musicTracks;
    AudioSource g;

    bool startedPlaying = false;

    void Awake()
    {
        if (!isMenu && GameManager.gameManager.gameState == GameManager.GameState.practice) this.gameObject.SetActive(false);//don't play music in practice mode.
        else
        {
            int r = Random.Range(0,musicTracks.Length);
            g = GetComponent<AudioSource>();
            g.clip = musicTracks[r];
        }
    }

    void Update()
    {
        if (!isMenu && !startedPlaying && !EventManager.eventManager.awaitingCountdown)
        {
            startedPlaying = true;
            g.Play();
        }
        else if (!startedPlaying && isMenu)
        {
            startedPlaying = true;
            g.Play();
        }
    }
}
