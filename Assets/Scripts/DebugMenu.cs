﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugMenu : MonoBehaviour
{
    //pointerVal: 1=CarSelect, 2=TrackSelect, 3=GameTypeSelect
    int pointerVal = 1;
    bool hasMoved = false;
    //carID: 1=MuscleCar
    int carID = 1;
    //selectedTrackID: 1=Test, 2=StLouis
    int trackID = 1;
    int skinID = 1;
    //bool psx = false;
    int lastRememberedRaceTrackID;
    int lastRmemberedStuntTrackID;

    public Text carNameText;
    public Text trackNameText;
    public Text gameTypeNameText;
    public Text skinIDText;

    public Text splashText;
    public string[] splashTextValues;
    float timeSplashUpdated;

    public GameObject muscleCarObject;
    public MeshRenderer muscleCarBodyRenderer;
    public Material[] muscleCarSkins;

    public AudioSource menuSource;
    public AudioClip cursorClip;
    public AudioClip cursorYesClip;
    public AudioClip cursorNoClip;
    public AudioClip confirmClip;
    public AudioClip errorClip;
    public AudioClip cheatClip;

    public Sprite beginnerSprite;
    public Sprite normalSprite;
    public Sprite expertSprite;
    public Sprite extremeSprite;

    public Image difficultyDisplay;
    public GameObject communityContentDisplay;

    void Awake()
    {
        GameManager.gameManager.player1selectedCarID = 1;
        GameManager.gameManager.player1selectedSkinID = 1;
        GameManager.gameManager.selectedTrackID = 4;
        GameManager.gameManager.gameState = GameManager.GameState.practice;
    }

    void Update()
    {
        /////UP-DOWN NAVIGATION/////
        //up
        if (!hasMoved && Input.GetAxis("MenuVertical")>0.5f && pointerVal == 1)
        {
            pointerVal = 4;
            hasMoved = true;
            carNameText.color = Color.blue;
            skinIDText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        else if (!hasMoved && Input.GetAxis("MenuVertical")>0.5f && pointerVal == 2)
        {
            pointerVal = 1;
            hasMoved = true;
            trackNameText.color = Color.blue;
            carNameText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        else if (!hasMoved && Input.GetAxis("MenuVertical")>0.5f && pointerVal == 3)
        {
            pointerVal = 2;
            hasMoved = true;
            gameTypeNameText.color = Color.blue;
            trackNameText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        else if (!hasMoved && Input.GetAxis("MenuVertical")>0.5f && pointerVal == 4)
        {
            pointerVal = 3;
            hasMoved = true;
            skinIDText.color = Color.blue;
            gameTypeNameText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        //down
        else if (!hasMoved && Input.GetAxis("MenuVertical")<-0.5f && pointerVal == 1)
        {
            pointerVal = 2;
            hasMoved = true;
            carNameText.color = Color.blue;
            trackNameText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        else if (!hasMoved && Input.GetAxis("MenuVertical")<-0.5f && pointerVal == 2)
        {
            pointerVal = 3;
            hasMoved = true;
            trackNameText.color = Color.blue;
            gameTypeNameText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        else if (!hasMoved && Input.GetAxis("MenuVertical")<-0.5f && pointerVal == 3)
        {
            pointerVal = 4;
            hasMoved = true;
            gameTypeNameText.color = Color.blue;
            skinIDText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }
        else if (!hasMoved && Input.GetAxis("MenuVertical")<-0.5f && pointerVal == 4)
        {
            pointerVal = 1;
            hasMoved = true;
            skinIDText.color = Color.blue;
            carNameText.color = Color.yellow;
            menuSource.clip = cursorClip;
            menuSource.Play();
        }

        /////LEFT-RIGHT VALUE INPUT/////
        //left
        ////CarSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")<-0.5f && pointerVal == 1)
        {
            if (carID == 1)
            {
                carID = 1;
                GameManager.gameManager.player1selectedCarID = carID;
                carNameText.text = "MUSCLE CAR";
                hasMoved = true;

                muscleCarObject.SetActive(false);
                muscleCarObject.SetActive(true);

                if (skinID > muscleCarSkins.Length) skinID = muscleCarSkins.Length;
                muscleCarBodyRenderer.material = muscleCarSkins[skinID-1];
                //if (muscleCarSkinIsCc[skinID-1]) communityContentDisplay.SetActive(true);
                //else communityContentDisplay.SetActive(false);
                menuSource.clip = cursorYesClip;
                menuSource.Play();
                difficultyDisplay.sprite = expertSprite;
                skinIDText.text = skinID + "/" + muscleCarSkins.Length;
            }
        }
        ////TrackSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")<-0.5f && pointerVal == 2)
        {
            if (trackID == 1)
            {
                trackID = 2;
                GameManager.gameManager.selectedTrackID = trackID;
                trackNameText.text = "ST LOUIS";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
            else if (trackID == 2)
            {
                trackID = 1;
                GameManager.gameManager.selectedTrackID = trackID;
                trackNameText.text = "TEST";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
        }
        ////GameTypeSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")<-0.5f && pointerVal == 3)
        {
            if (GameManager.gameManager.gameState == GameManager.GameState.practice)
            {
                GameManager.gameManager.gameState = GameManager.GameState.stunt;
                gameTypeNameText.text = "STUNT";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
                
            }
            else if (GameManager.gameManager.gameState == GameManager.GameState.stunt)
            {
                GameManager.gameManager.gameState = GameManager.GameState.singleRace;
                gameTypeNameText.text = "SINGLE RACE";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.gameState == GameManager.GameState.singleRace)
            {
                GameManager.gameManager.gameState = GameManager.GameState.practice;
                gameTypeNameText.text = "PRACTICE";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
                
            }
        }
        //SkinSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")<-0.5f && pointerVal == 4)
        {
            hasMoved = true;

            if (carID == 1)
            {
                if (skinID == 1) skinID = muscleCarSkins.Length;
                else skinID -= 1;
                muscleCarBodyRenderer.material = muscleCarSkins[skinID-1];
                //if (muscleCarSkinIsCc[skinID-1]) communityContentDisplay.SetActive(true);
                //else communityContentDisplay.SetActive(false);
                skinIDText.text = skinID + "/" + muscleCarSkins.Length;
            }
            GameManager.gameManager.player1selectedSkinID = skinID;
            menuSource.clip = cursorYesClip;
            menuSource.Play();
        }
        //RIGHT/////////////////////////////////////////////////////////////////////////////////////////////////////
        ////CarSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")>0.5f && pointerVal == 1)
        {
            if (carID == 1)
            {
                carID = 1;
                GameManager.gameManager.player1selectedCarID = carID;
                carNameText.text = "MUSCLE CAR";
                hasMoved = true;

                muscleCarObject.SetActive(false);
                muscleCarObject.SetActive(true);

                if (skinID > muscleCarSkins.Length) skinID = muscleCarSkins.Length;
                muscleCarBodyRenderer.material = muscleCarSkins[skinID-1];
                //if (muscleCArSkinIsCc[skinID-1]) communityContentDisplay.SetActive(true);
                //else communityContentDisplay.SetActive(false);
                menuSource.clip = cursorYesClip;
                menuSource.Play();
                difficultyDisplay.sprite = expertSprite;
                skinIDText.text = skinID + "/" + muscleCarSkins.Length;
            }
        }
        ////TrackSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")>0.5f && pointerVal == 2)
        {
            if (trackID == 1)
            {
                trackID = 2;
                GameManager.gameManager.selectedTrackID = trackID;
                trackNameText.text = "ST LOUIS";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
            else if (trackID == 2)
            {
                trackID = 1;
                GameManager.gameManager.selectedTrackID = trackID;
                trackNameText.text = "TEST";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
        }
        ////GameTypeSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")>0.5f && pointerVal == 3)
        {
            if (GameManager.gameManager.gameState == GameManager.GameState.practice)
            {
                GameManager.gameManager.gameState = GameManager.GameState.singleRace;
                gameTypeNameText.text = "SINGLE RACE";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.gameState == GameManager.GameState.singleRace)
            {
                GameManager.gameManager.gameState = GameManager.GameState.stunt;
                gameTypeNameText.text = "STUNT";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.gameState == GameManager.GameState.stunt)
            {
                GameManager.gameManager.gameState = GameManager.GameState.practice;
                gameTypeNameText.text = "PRACTICE";
                hasMoved = true;
                menuSource.clip = cursorYesClip;
                menuSource.Play();
            }
        }
        ////SkinSelect
        if (!hasMoved && Input.GetAxis("MenuHorizontal")>0.5f && pointerVal == 4)
        {
            hasMoved = true;

            if (carID == 1)
            {
                if (skinID == muscleCarSkins.Length) skinID = 1;
                else skinID += 1;
                muscleCarBodyRenderer.material = muscleCarSkins[skinID-1];
                //if (muscleCarSkinIsCc[skinID-1]) communityContentDisplay.SetActive(true);
                //else communityContentDisplay.SetActive(false);
                skinIDText.text = skinID + "/" + muscleCarSkins.Length;
            }
            GameManager.gameManager.player1selectedSkinID = skinID;
            menuSource.clip = cursorYesClip;
            menuSource.Play();
        }

        //Press A to start
        if (Input.GetButtonDown("Submit1"))
        {
            StartEvent();
        }
        //reset movement when release input
        if (hasMoved && Input.GetAxis("MenuVertical")<0.3f && Input.GetAxis("MenuVertical")>-0.3f && Input.GetAxis("MenuHorizontal")<0.3f && Input.GetAxis("MenuHorizontal")>-0.3f) hasMoved = false;

        //escape to quit
        if (Input.GetKeyDown("escape"))
        {
            Debug.LogError("QUIT");
            Application.Quit();
        }

        /////SPLASH TEXT/////
        if (Time.time > timeSplashUpdated + 12)
        {
            int randomSplash = Random.Range(0,splashTextValues.Length);
            splashText.text = splashTextValues[randomSplash];
            timeSplashUpdated = Time.time;
        }

        /////TEMP DEBUG KEYS/////
        if (Input.GetKeyDown("k"))//drones cheat
        {
            if (GameManager.gameManager.racerCount == 8)
            {
                GameManager.gameManager.racerCount = 16;
                splashText.text = "Drones set to 16!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.racerCount == 16)
            {
                GameManager.gameManager.racerCount = 32;
                splashText.text = "Drones set to 32!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.racerCount == 32)
            {
                GameManager.gameManager.racerCount = 64;
                splashText.text = "Drones set to 64! WARNING: Loud noises, bugs, and performance issues are likely!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.racerCount == 64)
            {
                GameManager.gameManager.racerCount = 128;
                splashText.text = "Drones set to 128! WARNING: Loud noises, bugs, and performance issues are likely!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.racerCount == 128)
            {
                GameManager.gameManager.racerCount = 1;
                splashText.text = "Drones set to 0!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.racerCount == 1)
            {
                GameManager.gameManager.racerCount = 8;
                splashText.text = "Drones set to 8!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
        }
        if (Input.GetKeyDown("l"))//laps cheat
        {
            if (GameManager.gameManager.selectedMaxLaps == 3)
            {
                GameManager.gameManager.selectedMaxLaps = 4;
                splashText.text = "Race laps set to 4!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 4)
            {
                GameManager.gameManager.selectedMaxLaps = 5;
                splashText.text = "Race laps set to 5!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 5)
            {
                GameManager.gameManager.selectedMaxLaps = 6;
                splashText.text = "Race laps set to 6!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 6)
            {
                GameManager.gameManager.selectedMaxLaps = 7;
                splashText.text = "Race laps set to 7!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 7)
            {
                GameManager.gameManager.selectedMaxLaps = 8;
                splashText.text = "Race laps set to 8!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 8)
            {
                GameManager.gameManager.selectedMaxLaps = 1;
                splashText.text = "Race laps set to 1!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 1)
            {
                GameManager.gameManager.selectedMaxLaps = 2;
                splashText.text = "Race laps set to 2!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
            else if (GameManager.gameManager.selectedMaxLaps == 2)
            {
                GameManager.gameManager.selectedMaxLaps = 3;
                splashText.text = "Race laps set to 3 (default)!";
                menuSource.clip = cheatClip;
                menuSource.Play();
            }
        }
    }

    void StartEvent()
    {
        //prevent loading unimplemented gamemode types
        if (GameManager.gameManager.gameState == GameManager.GameState.circuitRace || GameManager.gameManager.gameState == GameManager.GameState.battle || GameManager.gameManager.gameState == GameManager.GameState.obstacle || GameManager.gameManager.gameState == GameManager.GameState.tournamentRace)
        {
            menuSource.clip = errorClip;
            menuSource.Play();
            splashText.text = "INVALID EVENT SETTINGS ERROR: UNIMPLEMENTED GAMETYPE HAS BEEN SET ACTIVE!";
        }
        else if (trackID == 18 && (GameManager.gameManager.gameState == GameManager.GameState.practice || GameManager.gameManager.gameState == GameManager.GameState.singleRace))
        {
            splashText.text = "INVALID EVENT SETTINGS ERROR: map" + trackID + " CAN NOT BE USED WITH SELECTED GAMETYPE: " + GameManager.gameManager.gameState;
            menuSource.clip = errorClip;
            menuSource.Play();
        }
        else
        {
            menuSource.clip = confirmClip;
            menuSource.Play();
            SceneManager.LoadScene(trackID);
        }
    }
}
