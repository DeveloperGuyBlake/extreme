﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    public GameObject[] vehicleList;

    public enum GameState {practice=0, singleRace=1, circuitRace=2, stunt=3, battle=4, obstacle=5, tournamentRace=6}
    public GameState gameState = GameState.practice;
    public bool permaDeath = false;

    public bool showCommContent = false;//whether or not community content should be shown //TODO: IMPLEMENT THIS
    public int patronLevel = 0;//the donation tier of this player on Patreon

    //playercarID: 1=Sedan, 2=Bandit, 3=Coupe, 4=Exotic, 5=Van, 6=Sportster, 7=Subcompact, 8=Concept, 9=Pickup, 10=Compact, 11=MuscleCar, 12=Mobster, 13=Hatchback, 14=Cruiser, 15=Stallion, 16=Offroad, 17=Taxi, 18=HotRod, 19=Formula, 20=Prototype, 21=Rocket, 22=Dragster, 23=RocketZX, 24=Magnum, 25=Formula1, 26=EightBall, 27=SuperGT, 28=Bruiser, 26=GX2, 27=Mini, 28=EuroLX, 29=Venom, 30=Crusher, 31=Panther, 32=Sled, 33=Rodster
    public int player1selectedCarID = 1;
    //player2carID
    //player3carID
    //player4carID
    public int player1selectedSkinID = 1;
    public int playerCount = 1;
    public int racerCount = 8;//how many cars will appear in the race including players. (Used for drone spawning.) Do not set higher than number of spawn points in map else will throw IOOR exception.
    public int selectedTrackID = 1;//selectedTrackID: 0=Menu, 1=GoldenGate, 2=Embarcadero 3,4,5,6,7, 8=LasVegas, 8=NewYork1, 9=Hawaii, 10=NewYork2, 11=LosAngeles, 12=Seattle, 13=HalfPipe, 14=Crash, 15=Pipe, 16=Midway, 17=Marina, 18=Haight, 19=Civic, 20=Metro, 21=Mission, 22=Presidio, 23=Classic, 24=TheRim, 25=Disco, 26=Oasis, 27=Warehouse
    public int selectedMaxLaps = 3;
    //public int selectedDroneCount = 7;

    void Awake()
    {
        if (gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else Destroy(this);
    }
}
