﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneScript : MonoBehaviour
{
    public Transform chaseTarget;

    CarController cc;
    Rigidbody rb;

    public float steer;//The direction the AI intends to steer. Is public so that CarController can see it.
    public float throttle;
    public float brake;

    public float targetSpeed;//the speed the drone will slow down to if targetSpeedMet is false.
    public bool targetSpeedMet = true;//if true, drone will accelerate past targetSpeed. If false, drone will slow until speed is met (this value will be set to true) then drone will accelerate again.
    public bool finishedRace = false;//if this drone finished the race before the player (used to stop driving)

    void Awake()
    {
        cc = GetComponent<CarController>();
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (finishedRace || EventManager.eventManager.awaitingCountdown || EventManager.eventManager.gameOver)//AI stop if they finish the race before the player or if the player finished the race.
        {
            steer = 0;
            throttle = 0;
            brake = 0.5f;
            return;
        }

        if (chaseTarget == null)
        {
            Debug.LogError(this.gameObject.name + " has no assigned chaseTarget!");
            return;
        }

        Vector3 targetDirection = (chaseTarget.transform.position - this.transform.position).normalized; 
        steer = Vector3.Dot(targetDirection, this.transform.right);//steer = direction to object compared to our Right. 1 = 90 degrees to the right and -1 = 90 degrees to the left.
        steer = Mathf.Clamp(steer *= 2.5f, -1, 1);//multiply steering by how fast we're going so faster cars will turn harder and slower cars won't turn as hard. This prevents newly respawned cars from hitting the inside wall.
        //if (isStuck) steer *= -1;//swap steering angle when reversing from a wall so we face our target after leaving the wall. (TODO: Battle mode only, in race we should abort instead).//TODO: if isStuck and gamemode == race, abort.

        if (targetSpeedMet)
        {
            throttle = 1;
            brake = 0;
        }
        else if (!targetSpeedMet && Mathf.Abs(rb.transform.InverseTransformDirection(rb.velocity).z) <= targetSpeed)
        {
            targetSpeedMet = true;
            throttle = 1;
            brake = 0;
        }
        else if (!targetSpeedMet && Mathf.Abs(rb.transform.InverseTransformDirection(rb.velocity).z) > targetSpeed)
        {
            throttle = 0;
            brake = 0.8f;
            //TODO: BRAKE LIGHTS IN CARCONTROLLER
        }
        else Debug.LogError("Logic error in Drone targetSpeed throttle/brake logic! No conditions met!");
    }
}
