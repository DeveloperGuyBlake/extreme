﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    CarController cc;

    public float throttle;
    public float steer;
    public float brake;
    //public float reverse;//Is the player in reverse (0 or 1)? This inverts the throttle torque direction. (Hold reverse input and throttle to reverse.)

    public float deadZone = 0.1f;  //steering deadzone value. Math won't start at 0 when this value is reached. It will jump from 0 directly to this value.

    int playerID;

    void Start()
    {
        cc = GetComponent<CarController>();
        playerID = cc.playerID;
    }

    void Update()
    {
        if (EventManager.eventManager.paused) return;
        
        if (EventManager.eventManager.gameOver || EventManager.eventManager.awaitingCountdown)//TODO: Catalog and optimize all these redundant checks for gameOver, awaitingCountdown, and exploded between scripts.
        {
            throttle = 0;
            steer = 0;
            brake = 0.5f;
            return;
        }

        throttle = Input.GetAxis("Accel" + playerID);
        steer = Input.GetAxisRaw("Steering" + playerID);
        brake = Input.GetAxis("Brake" + playerID);

        if (steer <= deadZone && steer >= (deadZone * -1))  //steering input deadzone
        {
            steer = 0;
        }
        
        if (Input.GetButtonDown("Camera" + playerID))
        {
            cc.CycleCamera();
        }
    }
}
