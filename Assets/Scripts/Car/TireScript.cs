﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TireScript : MonoBehaviour
{
    WheelCollider wheel;
    CarController cc;
    //public float forwardSlip = 0.2f;//the amount of forward slip at which the tire starts to emit trails
    //public float sidewaysSlip = 0.5f;//the amount of sideways slip at which the tire starts to emit trails
    [HideInInspector]
    public float slipMagnitude;//the current overall average slip which is used for tire sound

    void Awake()
    {
        wheel = GetComponent<WheelCollider>();
        cc = GetComponentInParent<CarController>();
        //Debug.Log(cc);
    }
    
    void FixedUpdate()
    {
        WheelHit hit = new WheelHit();
        if (wheel.GetGroundHit(out hit) && !cc.exploded) slipMagnitude = (hit.forwardSlip+hit.sidewaysSlip)/2;
        else slipMagnitude = 0;
    }
}
