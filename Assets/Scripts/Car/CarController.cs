using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarController : MonoBehaviour
{
    public float enginePower = 70000;
    public float finalDrive = 5;
    public float[] gearVelocities;//The rb velocity at which the gear maxes out. Index 0 should always be a value of 1 to avoid divide by 0 errors! Do not include reverse gear (reverse is first gear * -1).;
    public float wingsForce = 1000;

    float wheelRPM;
    float loDiff;
    float loGTR;
    float hiDiff;
    float hiGTR;
    [Space]


    [Header("Physics Settings")]//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public float maxTurn = 20;
    [Tooltip("The minimum speed the vehicle must achieve for the tires to grip in Meters/Second. Default 2")]
    public float minGripSpeed = 2;
    public Transform com;
    public enum HandlingModel {Beginner, Normal, Expert, Extreme};
    public HandlingModel handlingModel;
    [Tooltip("The distance to offset wheel travel so wheel mesh appears to follow suspension travel in the correct position.")]
    public float wheelMeshTravelOffset = 0; //The radius of the wheel collider: used to offset wheelMesh position so wheels appear in the correct location
    public List<WheelCollider> throttleWheels;
    public List<WheelCollider> steeringWheels; //list of wheels that can steer
    public List<GameObject> throttleWheelMeshes;//used only for disabling of wheel meshes in hood camera and respawn anim sequence.//TODO: These are redundant! Meshes are already listed below.
    public List<GameObject> steeringWheelMeshes;//used only for disabling of wheel meshes in hood camera and respawn anim sequence.
    public List<GameObject> steeringWheelPivots;//used for rotating wheel meshes correctly
    public GameObject frontLeftWheelMesh;
    public GameObject frontRightWheelMesh;
    public GameObject backLeftWheelMesh;
    public GameObject backRightWheelMesh;
    public GameObject carToCarColObject;
    public WheelCollider frontLeftWheelCollider;//list of meshes and colliders for setting mesh visual rotation to represent collider speed.
    public WheelCollider frontRightWheelCollider;
    public WheelCollider backLeftWheelCollider;
    public WheelCollider backRightWheelCollider;
    public TireScript frontLeftTireScript;
    public TireScript frontRightTireScript;
    public TireScript backLeftTireScript;
    public TireScript backRightTireScript;
    public float gripAsymSlip;
    public float gripAsymSlipVal;
    public float gripExtSlip;
    public float gripExtSlipVal;
    public float gripStiff;
    public float slipAsymSlip;
    public float slipAsymSlipVal;
    public float slipExtSlip;
    public float slipExtSlipVal;
    public float slipStiff;
    public float steerGripAsymSlip;
    public float steerGripAsymSlipVal;
    public float steerGripExtSlip;
    public float steerGripExtSlipVal;
    public float steerGripStiff;
    public float steerSlipAsymSlip;
    public float steerSlipAsymSlipVal;
    public float steerSlipExtSlip;
    public float steerSlipExtSlipVal;
    public float steerSlipStiff;


    Vector3 frontRightWheelMeshStartPos;
    Vector3 frontLeftWheelMeshStartPos;
    Vector3 backRightWheelMeshStartPos;
    Vector3 backLeftWheelMeshStartPos;
    Vector3 frontRightSkidStartPos;
    Vector3 frontLeftSkidStartPos;
    Vector3 backRightSkidStartPos;
    Vector3 backLeftSkidStartPos;
    float tireSlipMagnitude;
    bool isGripModel = true;
    bool releaseHandbrake = false;//if we released the handbrake recently, for running a check on if we should grip up tires
    float minGripAngle;
    [Space]

    [Header("Damage/Explosion Settings")]/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public GameObject bodyModel;
    public GameObject explodedModel;
    public ParticleSystem explodeParticle;
    public ParticleSystem fireParticle;
    public float physicsForceTolerance = 2;//how much force the car can tolerate before exploding.
    public GameObject fireLight;

    float explodeTime;//what time the explosion started.
    float explodeDuration = 4;//How many seconds the car will remain exploded before respawning. TODO: Inheret from gameManager game settings. (Better yet, remove this variable and check gameManager value when exploded.)
    [Space]

    [Header("Interface/Camera")]/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public GameObject chaseCamTarget;
    public GameObject hoodCamTarget;
    public GameObject hoodLookBackTarget;
    public GameObject explodeTarget;//the target the camera will follow while the car is exploded.

    public SmoothFollow camScript;
    public Text gearText;
    public Text currentLapText;
    public Text bestLapText;
    public Animator difficultyContainerAnimator;
    public GameObject debugUiObject;
    public Text debugCheckpointHitText;
    public Text debugCheckpointCurrentText;
    public Text lapText;
    [Tooltip("If true, wheel meshes won't be hidden in hood camera. Useful for open-wheeled cars.")]
    public bool dontHideWheelMeshes = false;

    bool lookBack = false;
    bool ebrake = false; //Whether hand brake is being pulled.
    float startDistance;
    float startHeight;
    float currentLapStartTime;
    float currentLapElapsedTime;
    float bestLapTime = 999999;
    [Space]
    
    [Header("Lights/Materials/Anim")]/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Material[] skins;
    public Animator wingsAnimator;
    public MeshRenderer bodyMeshRenderer;
    public Texture dayEmitIdleTex;
    public Texture nightEmitIdleTex;
    public Texture dayEmitBrakeTex;
    public Texture nightEmitBrakeTex;
    public GameObject[] headlights;

    bool useHeadlights = false;
    bool brakeLightsOn = false;
    [Space]

    [Header("Audio")]/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public AudioSource explodeAudio;
    public AudioSource tireAudioSource;
    public AudioSource engineAudioSource;
    public AudioSource physicsAudioSource;
    public float enginepitch = 0.4f;
    public AudioClip respawnClip;
    public AudioClip bumpClip;
    public AudioClip[] physicsClips;
    [Space]
    
    [Header("AUTO - NO EDIT")]/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //[HideInInspector]
    public InputManager Im;
    //[HideInInspector]
    public DroneScript ds;
    //[HideInInspector]
    public Rigidbody rb;
    //[HideInInspector]
    public int playerID;//Player1 is ID 1. There is no car with playerID 0.
    //[HideInInspector]
    public bool exploded = false; //LaserScript, KillTrigger, other CarControllers trying to aim at us, and weaponPickups use this.
    //[HideInInspector]
    public bool canExplode = false;//This prevents exploding in loops or while in the air.

    int currentGear = 1;
    float rbStartMass;
    int camValue = 1;
    public bool isAI = false;
    float driveWheelCircumference;
    bool wings = false;
    bool attemptWings = false;
    bool airborne = false;
    bool airborneStepComplete = false;
    float launchRot;
    bool airStopComplete = false;
    bool debugUI = false;
    bool volSet = false;//gets set to true when AI have set their engine volume to 0 to prevent setting it every frame.
    bool timerRunning = false;//wether the lap timer is running or not. Becomes true when the time starts by crossing the line on lap 1.
    bool invincible = false;
    float angVelMagVal = 8;//Car will only explode if angular velocity magnitude is lower than this value at the time of impact. This is automatically changed in stunt mode to prevent explosions during stunts.



    void Awake()
    {
        if (gearVelocities.Length == 0) Debug.LogError(this.gameObject.name + " gearVelocities has not be set up!");

        if (GameManager.gameManager.gameState == GameManager.GameState.stunt)
        {
            invincible = true;//TODO: ADD INVINCIBILITY CHEAT CHECK HERE
            angVelMagVal = 1;
        }

        rb = GetComponent<Rigidbody>();
        rbStartMass = rb.mass;
        rb.centerOfMass = com.localPosition;
        frontRightWheelMeshStartPos = frontRightWheelMesh.transform.localPosition;
        frontLeftWheelMeshStartPos = frontLeftWheelMesh.transform.localPosition;
        backRightWheelMeshStartPos = backRightWheelMesh.transform.localPosition;
        backLeftWheelMeshStartPos = backLeftWheelMesh.transform.localPosition;

        //TODO: ExplodeDuration = GameManager.gameManager.explodeDuration (should be changeable in options)

        if (handlingModel == HandlingModel.Beginner) minGripAngle = 0.7f;
        if (handlingModel == HandlingModel.Normal) minGripAngle = 0.75f;
        if (handlingModel == HandlingModel.Expert) minGripAngle = 0.8f;
        if (handlingModel == HandlingModel.Extreme) minGripAngle = 0.9f;//TODO: CHANGE THIS TO WORK WITH ANY NEW SYSTEM?
        
        StartCoroutine(DroneCheck());

        //TODO: Get selected skin based on this playerID's selected skin in GameManager. Use skins[] array above to choose material matching selected skin value.
        //TODO: Get selected rims based on this playerID's selected rims in GameManager.
        //TODO: Get selected wing skin based on this playerID's selected wing skin in GameManager.

        //TODO: SET UP TEAMS
    }




    void FixedUpdate()
    {
        float vel = rb.velocity.magnitude;
        //////////Move wheelmesh based on suspension travel//////////
        RaycastHit hitFR;
        RaycastHit hitFL;
        RaycastHit hitBR;
        RaycastHit hitBL;

        if (!isAI )//TODO: REIMPLEMENT THIS ON ai CARS WHEN IT DOESN'T KILL FRAMERATE
        {
            if (!exploded && Physics.Raycast(frontRightWheelCollider.transform.position, -frontRightWheelCollider.transform.up, out hitFR, (frontRightWheelCollider.suspensionDistance*1.1f)+(frontRightWheelCollider.radius), ~11, QueryTriggerInteraction.Ignore))
            {
                frontRightWheelMesh.transform.position = hitFR.point + (transform.up*wheelMeshTravelOffset);
                //Debug.DrawLine(frontRightWheelCollider.transform.position, hitFR.point, Color.green);
                //Debug.Log("hit: " + hit.collider.gameObject.name);
            }
            else
            {
                frontRightWheelMesh.transform.localPosition = frontRightWheelMeshStartPos;
                //Debug.DrawLine(frontRightWheelCollider.transform.position, frontRightWheelCollider.transform.position + new Vector3(0,(frontRightWheelCollider.suspensionDistance+(frontRightWheelCollider.radius*-1,0)), Color.red);
                //Debug.Log("Did not hit");
            }
            if (!exploded && Physics.Raycast(frontLeftWheelCollider.transform.position, -frontLeftWheelCollider.transform.up, out hitFL, (frontLeftWheelCollider.suspensionDistance*1.1f)+(frontLeftWheelCollider.radius), ~11, QueryTriggerInteraction.Ignore))
            {
                frontLeftWheelMesh.transform.position = hitFL.point + (transform.up*wheelMeshTravelOffset);
                //Debug.DrawLine(frontLeftWheelCollider.transform.position, hitFL.point, Color.green);
            } 
            else
            {
                frontLeftWheelMesh.transform.localPosition = frontLeftWheelMeshStartPos;
            }
            if (!exploded && Physics.Raycast(backRightWheelCollider.transform.position, -backRightWheelCollider.transform.up, out hitBR, (backRightWheelCollider.suspensionDistance*1.1f)+(backRightWheelCollider.radius), ~11, QueryTriggerInteraction.Ignore))
            {
                backRightWheelMesh.transform.position = hitBR.point + (transform.up*wheelMeshTravelOffset);
                //Debug.DrawLine(backRightWheelCollider.transform.position, hitBR.point, Color.green);
            } 
            else
            {
                backRightWheelMesh.transform.localPosition = backRightWheelMeshStartPos;
            }
            if (!exploded && Physics.Raycast(backLeftWheelCollider.transform.position, -backLeftWheelCollider.transform.up, out hitBL, (backLeftWheelCollider.suspensionDistance*1.1f)+(backLeftWheelCollider.radius), ~11, QueryTriggerInteraction.Ignore))
            {
                backLeftWheelMesh.transform.position = hitBL.point + (transform.up*wheelMeshTravelOffset);
                //Debug.DrawLine(backLeftWheelCollider.transform.position, hitBL.point, Color.green);
            } 
            else
            {
                backLeftWheelMesh.transform.localPosition = backLeftWheelMeshStartPos;
            }
        }


        ////////////////////////spin wheelMeshes based on wheelcollider speed/////////////////////////////////////////
        frontRightWheelMesh.transform.localEulerAngles += Vector3.right * frontRightWheelCollider.rpm * 360 / 60 * Time.fixedDeltaTime;  //TODO: FIX GIMBAL LOCK
        frontLeftWheelMesh.transform.localEulerAngles += Vector3.right * frontLeftWheelCollider.rpm * 360 / 60 * Time.fixedDeltaTime;
        backRightWheelMesh.transform.localEulerAngles += Vector3.right * backRightWheelCollider.rpm * 360 / 60 * Time.fixedDeltaTime;
        backLeftWheelMesh.transform.localEulerAngles += Vector3.right * backLeftWheelCollider.rpm * 360 / 60 * Time.fixedDeltaTime;



        ///////////////////////////////Handle Tire Sound///////////////////////////////
        tireSlipMagnitude = (Mathf.Abs(frontLeftTireScript.slipMagnitude) + Mathf.Abs(frontRightTireScript.slipMagnitude) + Mathf.Abs(backLeftTireScript.slipMagnitude) + Mathf.Abs(backRightTireScript.slipMagnitude))/4;

        //Debug.Log(Mathf.Clamp(tireSlipMagnitude*4+0.3f,0.5f,1.3f)); //DEBUG average tireSlip
        if (tireSlipMagnitude >= 0.125f)
        {
            tireAudioSource.volume = tireSlipMagnitude;
            tireAudioSource.pitch = Mathf.Clamp(tireSlipMagnitude*4+0.3f,0.5f,1.3f);
        }
        else
        {
            tireAudioSource.volume = 0;
            tireAudioSource.pitch = 0;
        }


        /////AIRBORNE ASSISTS/////
        if (!exploded && !frontRightWheelCollider.isGrounded && !frontLeftWheelCollider.isGrounded && !backRightWheelCollider.isGrounded && !backLeftWheelCollider.isGrounded)
        {
            airborne = true;
            if (!airborneStepComplete) AirborneStep();
        }
        else
        {
            airborne = false;
            airborneStepComplete = false;
            airStopComplete = false;
        }
        if (airborne)
        {
            if (!airStopComplete && handlingModel == HandlingModel.Beginner)//TODO: Change these get and set angular velocities to local angular velocities instead of world?
            {
                if (Mathf.Abs(launchRot - this.gameObject.transform.rotation.y) > 0.05f)//BEGINNER
                {
                    rb.angularVelocity = new Vector3(rb.angularVelocity.x, 0, rb.angularVelocity.z);
                    airStopComplete = true;
                }
            }
            else if (!airStopComplete && handlingModel == HandlingModel.Normal)
            {
                if (Mathf.Abs(launchRot - this.gameObject.transform.rotation.y) > 0.1f)//ADVANCED
                {
                    rb.angularVelocity = new Vector3(rb.angularVelocity.x, 0, rb.angularVelocity.z);//TODO: REPLACE SUDDEN STOP WITH ANGULAR DRAG, AND TIGHTEN STOP ROTATION ANGLES? (BEGINNER COULD ALWAYS HAVE ROTATION DAMP IN THE AIR? WOULD INTERFERE WITH STUNT THOUGH)
                    airStopComplete = true;
                }
            }
            else if (!airStopComplete && handlingModel == HandlingModel.Expert)
            {
                if (Mathf.Abs(launchRot - this.gameObject.transform.rotation.y) > 0.3f)//EXPERT
                {
                    rb.angularVelocity = new Vector3(rb.angularVelocity.x, 0, rb.angularVelocity.z);//TODO: REPLACE SUDDEN STOP WITH ANGULAR DRAG, AND TIGHTEN STOP ROTATION ANGLES? (BEGINNER COULD ALWAYS HAVE ROTATION DAMP IN THE AIR? WOULD INTERFERE WITH STUNT THOUGH)
                    airStopComplete = true;
                }
            }
        }
        //angular drag = 1;
        //else angular drag - 0.1f;


        ///////////////////////////////Explode if we're upside down///////////////////////////////
        if (!exploded && canExplode && (Vector3.Dot(transform.up, Vector3.down) > -0.1f) && vel < 16 && rb.angularVelocity.magnitude <= angVelMagVal) //A DOT of -1 = rightside up, 0 = 90 degrees, 1 = upside down.
        {
            Debug.Log(("AngleDot: " + Vector3.Dot(transform.up, Vector3.down)).ToString() + " | Velocity: " + vel + " | AngVelMag: " + rb.angularVelocity.magnitude);
            Explode(true);
        }


        ////////////////////////////////Handle player input////////////////////////////////
        if (!isAI && Im != null)
        {
            foreach (WheelCollider wheel in steeringWheels)
            {
                if (!wings && !EventManager.eventManager.gameOver && !exploded) wheel.steerAngle = maxTurn * Im.steer;
                else wheel.steerAngle = 0;//if driving disabled or exploded, steer straight.
            }
            foreach (GameObject wheelMesh in steeringWheelPivots)
            {
                Vector3 steerAngle = new Vector3(0,0,0);
                steerAngle.y += Im.steer*maxTurn;
                wheelMesh.transform.localEulerAngles = steerAngle;
            }

            /////WINGS/////
            if (wings)
            {
                Vector3 forceAngle = new Vector3(-Input.GetAxis("Vertical"+playerID),0,-Input.GetAxis("Steering"+playerID));
                if (forceAngle.magnitude >= 0.2f)
                {
                    rb.AddRelativeTorque(forceAngle * wingsForce * Time.fixedDeltaTime);
                }
            }

            /////E-BRAKE/////
            if (!exploded && Input.GetButton("Wings"+playerID) && (frontRightWheelCollider.isGrounded || frontLeftWheelCollider.isGrounded || backRightWheelCollider.isGrounded || backLeftWheelCollider.isGrounded)) ebrake = true;
            else ebrake = false;


            /////TRANSMISSION/////
            //Debug.Log(vel);
            float FDR = finalDrive*1000000;
            float wheelRPM = (backRightWheelCollider.rpm + backLeftWheelCollider.rpm) / 2;//TODO: APPLY SOME BRAKE TORQUE IF wheelRPM IS MUCH HIGHER THAN CURRENTGEAR VELOCITY.
            if (currentGear != 0) loDiff = wheelRPM - (gearVelocities[currentGear-1] - (gearVelocities[currentGear-1]*0.3f));
            hiDiff = wheelRPM - gearVelocities[currentGear];
            loGTR = Mathf.Pow(2f,-loDiff*loDiff/2f);
            hiGTR = Mathf.Clamp(Mathf.Pow(FDR,-hiDiff*hiDiff/FDR), 0.2f, 1);

            foreach (WheelCollider wheel in throttleWheels)
            {
                if (!EventManager.eventManager.gameOver && !exploded)
                {
                    //Debug.Log(Im.throttle);
                    if (currentGear == 0) wheel.motorTorque = (-enginePower * hiGTR) * Im.throttle * Time.fixedDeltaTime;//currentGear 0 means reverse
                    else if (wheelRPM > gearVelocities[currentGear-1] && wheelRPM < gearVelocities[currentGear]) wheel.motorTorque = enginePower * Im.throttle * Time.fixedDeltaTime;
                    else if (loGTR > hiGTR) wheel.motorTorque = (enginePower * loGTR) * Im.throttle * Time.fixedDeltaTime;
                    else if (hiGTR > loGTR) wheel.motorTorque = (enginePower * hiGTR) * Im.throttle * Time.fixedDeltaTime;


                    //else if (wheelRPM > gearVelocities[currentGear-1] && wheelRPM < gearVelocities[currentGear]) wheel.motorTorque = enginePower * Im.throttle * Time.fixedDeltaTime;//full power if between current and lower gearVelocities values.
                    //else if (loGTR > hiGTR) wheel.motorTorque = (enginePower * loGTR) * Im.throttle * Time.fixedDeltaTime;//if below torque band, use lower GearTorqueRatio.
                    //else if (hiGTR > loGTR) wheel.motorTorque = (enginePower * hiGTR) * Im.throttle * Time.fixedDeltaTime;//if above torque band, use higher GearTorqueRatio.
                    
                    if (ebrake) wheel.brakeTorque = 5000;
                    else wheel.brakeTorque = Im.brake * 5000;
                }
                else
                {
                    wheel.motorTorque = 0;
                    wheel.brakeTorque = 5000;
                }
            }
            float p = Mathf.Clamp((wheelRPM / gearVelocities[currentGear]) + enginepitch, enginepitch, 2);
            engineAudioSource.pitch = p;
            if (!isAI && !exploded) engineAudioSource.volume = Mathf.Clamp(hiGTR, 0.6f, 1);//TODO: Implement dynamic engine volume better
            else if (!isAI && exploded|| isAI && !volSet)
            {
                engineAudioSource.volume = 0;
                if (isAI) volSet = true;
            }
            
            //Debug transmission data:
            //if (wheelRPM > gearVelocities[currentGear-1] && wheelRPM < gearVelocities[currentGear]) Debug.Log("WheelRPM: "+wheelRPM+" | loDiff: "+loDiff+" | loGTR: "+loGTR+" | hiDiff: "+hiDiff+" | hiGTR: "+hiGTR+" |     USING FULL-POWER RATIO: 1");
            //else  if (loGTR > hiGTR) Debug.Log("WheelRPM: "+wheelRPM+" | loDiff: "+loDiff+" | loGTR: "+loGTR+" | hiDiff: "+hiDiff+" | hiGTR: "+hiGTR+" |     USING loGTR RATIO: "+ loGTR);
            //else  if (hiGTR > loGTR) Debug.Log("WheelRPM: "+wheelRPM+" | loDiff: "+loDiff+" | loGTR: "+loGTR+" | hiDiff: "+hiDiff+" | hiGTR: "+hiGTR+" |     USING hiGTR RATIO: "+ hiGTR);
        }
        else if (isAI)////AI only////
        {
            float FDR = finalDrive*1000000;
            float wheelRPM = (backRightWheelCollider.rpm + backLeftWheelCollider.rpm) / 2;//TODO: APPLY SOME BRAKE TORQUE IF wheelRPM IS MUCH HIGHER THAN CURRENTGEAR MAX VELOCITY.
            if (currentGear != 0) loDiff = wheelRPM - (gearVelocities[currentGear-1] - (gearVelocities[currentGear-1]*0.3f));
            hiDiff = wheelRPM - gearVelocities[currentGear];//TODO: Clean up redundant section (this section needs done for both players and drones)
            loGTR = Mathf.Pow(2f,-loDiff*loDiff/2f);
            hiGTR = Mathf.Clamp(Mathf.Pow(FDR,-hiDiff*hiDiff/FDR), 0.2f, 1);
            foreach (WheelCollider wheel in throttleWheels)//DRONE THROTTLE
            {
                if (!EventManager.eventManager.gameOver && !exploded)//TODO: Catalog and optimize all these redundant checks for gameOver, awaitingCountdown, and exploded between scripts.
                {
                    if (currentGear == 0) wheel.motorTorque = (-enginePower * ds.throttle * hiGTR) * Time.fixedDeltaTime;//currentGear 0 means reverse
                    else if (wheelRPM > gearVelocities[currentGear-1] && wheelRPM < gearVelocities[currentGear]) wheel.motorTorque = enginePower * ds.throttle * Time.fixedDeltaTime;
                    else if (loGTR > hiGTR) wheel.motorTorque = (enginePower * ds.throttle * loGTR) * Time.fixedDeltaTime;
                    else if (hiGTR > loGTR) wheel.motorTorque = (enginePower * ds.throttle * hiGTR) * Time.fixedDeltaTime;
                }
                else
                {
                    wheel.motorTorque = 0;
                    wheel.brakeTorque = 5000;
                }
                wheel.brakeTorque = ds.brake * 5000;//TODO: Redundant?
            }

            foreach (WheelCollider wheel in steeringWheels)//DRONE STEERING
            {
                if (!wings && !EventManager.eventManager.gameOver && !exploded) wheel.steerAngle = maxTurn * ds.steer;
                else wheel.steerAngle = 0;//if driving disabled or exploded, steer straight.
            }
            foreach (GameObject wheelMesh in steeringWheelMeshes)
            {
                Vector3 steerAngle = new Vector3(0,0,0);
                steerAngle.y += ds.steer*maxTurn;
                wheelMesh.transform.localEulerAngles = steerAngle;
            }
        }
        //if (isAI && hiGTR > 1.3f && currentGear != gearVelocities.Length) ShiftUp();//TODO: IMPLEMENT AUTO TRANSMISSION FOR PLAYER HERE BY INCLINDING "|| AUTOTRANSMISSION"
        //else if (isAI && loGTR < 0.5f && currentGear > 1) ShiftDown();

        if ((!isAI && Im != null) || (!isAI && ds != null))
        {
            if (!isAI)
            {
                if (!brakeLightsOn && Im.brake >= 0.1f)
                {
                    if (useHeadlights) bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitBrakeTex);//TODO: TEXTURE SHEET SO DIFFERENT SKINS CAN HAVE DIFFERENT LIGHTS?
                    else bodyMeshRenderer.material.SetTexture("_EmissionMap", dayEmitBrakeTex);
                    brakeLightsOn = true;
                }
                else if (brakeLightsOn && Im.brake < 0.1f)
                {
                    if (useHeadlights) bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitIdleTex);
                    else bodyMeshRenderer.material.SetTexture("_EmissionMap", dayEmitIdleTex);
                    brakeLightsOn = false;
                }
            }
            else if (isAI)
            {
                if (!brakeLightsOn && ds.brake >= 0.1f)
                {
                    if (useHeadlights) bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitBrakeTex);//TODO: TEXTURE SHEET SO DIFFERENT SKINS CAN HAVE DIFFERENT LIGHTS?
                    else bodyMeshRenderer.material.SetTexture("_EmissionMap", dayEmitBrakeTex);
                    brakeLightsOn = true;
                }
                else if (brakeLightsOn && ds.brake < 0.1f)
                {
                    if (useHeadlights) bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitIdleTex);
                    else bodyMeshRenderer.material.SetTexture("_EmissionMap", dayEmitIdleTex);
                    brakeLightsOn = false;
                }
            }
        }



        //////////TIRE GRIP SWITCH SYSTEM//////////
        Vector3 v = (transform.InverseTransformDirection(rb.velocity)); //GETS RB VELOCITY FORWARD
        float tot = Mathf.Abs(v.x) + Mathf.Abs(v.y) + Mathf.Abs(v.z);//v.z is the value of forward momentum
        float f = v.z / tot;//f is what percentage of all momentum is forward
        //Debug.Log("f" + f + " |z" + v.z);

        if (ebrake)
        {
            foreach (WheelCollider w in throttleWheels)
            {
                WheelFrictionCurve wfc = w.sidewaysFriction;
                wfc.asymptoteSlip = slipAsymSlip;
                wfc.asymptoteValue = slipAsymSlipVal;
                wfc.extremumSlip = slipExtSlip;
                wfc.extremumValue = slipExtSlipVal;
                wfc.stiffness = slipStiff;
                w.sidewaysFriction = wfc;
            }
        }
        if (releaseHandbrake || (!isGripModel && (v.z >= minGripSpeed && f >= minGripAngle)))//v.z is min speed to grip, f is percent of momentum forward required to grip
        {
            releaseHandbrake = false;
            Debug.Log("Grip");
            foreach (WheelCollider w in throttleWheels)
            {
                if (!ebrake)//only grip throttle wheels if ebrake isn't held
                {
                    WheelFrictionCurve wfc = w.sidewaysFriction;
                    wfc.asymptoteSlip = gripAsymSlip;
                    wfc.asymptoteValue = gripAsymSlipVal;
                    wfc.extremumSlip = gripExtSlip;
                    wfc.extremumValue = gripExtSlipVal;
                    wfc.stiffness = gripStiff;
                    w.sidewaysFriction = wfc;
                }
            }
            foreach (WheelCollider w in steeringWheels)
            {
                WheelFrictionCurve wfc = w.sidewaysFriction;
                wfc.asymptoteSlip = steerGripAsymSlip;
                wfc.asymptoteValue = steerGripAsymSlipVal;
                wfc.extremumSlip = steerGripExtSlip;
                wfc.extremumValue = steerGripExtSlipVal;
                wfc.stiffness = steerGripStiff;
                w.sidewaysFriction = wfc;
            }
            isGripModel = true;
            //Debug.Log("GRIP");
        }
        else if ((isGripModel && v.z < minGripSpeed) || (isGripModel && f < minGripAngle))//v.z is speed where less than this speed will slip, f is the percent of momentum forward required to grip
        {
            foreach (WheelCollider w in throttleWheels)
            {
                WheelFrictionCurve wfc = w.sidewaysFriction;
                wfc.asymptoteSlip = slipAsymSlip;
                wfc.asymptoteValue = slipAsymSlipVal;
                wfc.extremumSlip = slipExtSlip;
                wfc.extremumValue = slipExtSlipVal;
                wfc.stiffness = slipStiff;
                w.sidewaysFriction = wfc;
            }
            foreach (WheelCollider w in steeringWheels)
            {
                WheelFrictionCurve wfc = w.sidewaysFriction;
                wfc.asymptoteSlip = steerSlipAsymSlip;
                wfc.asymptoteValue = steerSlipAsymSlipVal;
                wfc.extremumSlip = steerSlipExtSlip;
                wfc.extremumValue = steerSlipExtSlipVal;
                wfc.stiffness = steerSlipStiff;
                w.sidewaysFriction = wfc;
            }
            isGripModel = false;
            //Debug.Log("SLIP");
        }


        if (exploded) tireAudioSource.volume = 0;//TODO: Fix tire audio properly. This is a crude and ineffecient fix for tire audio continuing to play while exploded.
    }




    void Update()
    {
        //if (!isAI) Debug.Log(rb.transform.InverseTransformDirection(rb.velocity).z);//Debug's player's forward velocity in meters/second.

        if (!GameManager.gameManager.permaDeath && exploded && Time.time > explodeTime + explodeDuration && !EventManager.eventManager.gameOver)//respawn if we've been dead for a while, but don't respawn if the game has ended.
        {
            Respawn();
        }

        if (isAI || camScript == null) return;

        //////////MANUAL SHIFTING//////////
        if (!isAI && !exploded && !EventManager.eventManager.gameOver && !EventManager.eventManager.paused && Input.GetButtonDown("ShiftUp" + playerID))
        {
            ShiftUp();
        }
        if (!isAI && !exploded && !EventManager.eventManager.gameOver && !EventManager.eventManager.paused && Input.GetButtonDown("ShiftDown" + playerID))
        {
            ShiftDown();
        }

        //////////HANDLE WINGS//////////
        if (!isAI && Input.GetButtonDown("Wings" + playerID))//if press button, ready wings
        {
            attemptWings = true;
            if (!exploded && !EventManager.eventManager.gameOver && airborne)//if already airborne, open wings
            {
                wings = true;
                wingsAnimator.SetTrigger("Open");
            }
        }
        else if (!isAI && airborne == false && wings == true)//if we land while wings are still active, close wings
        {
            wings = false;
            wingsAnimator.SetTrigger("Close");
        }
        else if (!isAI && !exploded && !EventManager.eventManager.gameOver && airborne && attemptWings && !wings)//if we jumped into the air while holding the wings button, open wings
        {
            //Debug.Log("jump with wings on");
            wings = true;
            wingsAnimator.SetTrigger("Open");
        }
        else if (!isAI && Input.GetButtonUp("Wings" + playerID))//if release button, unready wings
        {
            attemptWings = false;
            if (wings)                                                                        
            {
                wingsAnimator.SetTrigger("Close");
                wings = false;
            }
        }
        else if ((!isAI && exploded) || (!isAI &&EventManager.eventManager.gameOver))//if we're exploded or the game is over, close wings.
        {
            wings = false;
        }


        //////////CAMERA LOOK FUNCTIONS//////////
        if (!isAI && !exploded)
        {
            float lookHoriValue = Input.GetAxis("LookHori"+playerID);//LOOK LEFT/RIGHT/////
            if (camValue == 1 && lookHoriValue >= 0.1f || lookHoriValue <= -0.1f)
            {
                camScript.rotationDamping = 8;
                if (!lookBack) chaseCamTarget.transform.localRotation = Quaternion.Euler(0,lookHoriValue*90,0);
                else chaseCamTarget.transform.localRotation = Quaternion.Euler(0,180 + lookHoriValue*90,0);
            }
            else if (lookBack)
            {
                chaseCamTarget.transform.localRotation = Quaternion.Euler(0,180,0);
                camScript.rotationDamping = 3;
            }
            else
            {
                chaseCamTarget.transform.localRotation = Quaternion.Euler(0,0,0);
                camScript.rotationDamping = 3;
            }

            if (Input.GetButtonDown("LookBack"+playerID))//LOOK BACK/////
            {
                lookBack = true;
                if (camValue == 1)
                {
                    chaseCamTarget.transform.localRotation = Quaternion.Euler(0,180,0);
                    StartCoroutine("SnapCamRotation");
                }
                if (camValue == 2)
                {
                    camScript.customEnabledBool = false;
                    camScript.transform.position = hoodLookBackTarget.transform.position;
                    camScript.transform.rotation = hoodLookBackTarget.transform.rotation;
                    camScript.transform.SetParent(hoodLookBackTarget.transform);
                }
            }

            else if (Input.GetButtonUp("LookBack"+playerID))//LOOK FORWARD/////
            {
                lookBack = false;
                if (camValue == 1)
                {
                        chaseCamTarget.transform.localRotation = Quaternion.Euler(0,0,0);
                        StartCoroutine("SnapCamRotation");
                }
                if (camValue == 2)
                {
                    camScript.customEnabledBool = false;
                    camScript.transform.position = hoodCamTarget.transform.position;
                    camScript.transform.rotation = hoodCamTarget.transform.rotation;
                    camScript.transform.SetParent(hoodCamTarget.transform);
                }
            }
        }

        if (Input.GetButtonUp("Wings"+playerID)) releaseHandbrake = true;
        Debug.Log(releaseHandbrake);


        //////////LAP TIMER//////////
        if (!isAI && timerRunning) currentLapElapsedTime = (Time.time - currentLapStartTime);
        if (!isAI && timerRunning) currentLapText.text = currentLapElapsedTime.ToString();//TODO: TIMER DISPLAY INSTEAD OF SECONDS

        //////////TEMP DEBUG KEYS//////////
        if (!isAI && Input.GetKeyDown("p")) Explode(true);
        //if (!isAI && Input.GetKeyDown("o")) DifficultyCycle();
        if (!isAI && Input.GetKeyDown("i")) DebugUIToggle();
    }


    public void Explode(bool crashed)
    {
        if (!exploded)
        {
            explodeParticle.Play();
            fireParticle.Play();
            fireLight.SetActive(true);
            explodeAudio.Play();
            wingsAnimator.SetTrigger("Explode");
            wings = false;

            exploded = true;
            explodeTime = Time.time;
            rb.drag = 1;
            rb.angularDrag = 2;
            
            explodedModel.SetActive(true);
            bodyModel.SetActive(false);
            frontRightWheelMesh.SetActive(false);
            frontLeftWheelMesh.SetActive(false);
            backRightWheelMesh.SetActive(false);
            backLeftWheelMesh.SetActive(false);
            frontRightWheelCollider.gameObject.SetActive(false);
            frontLeftWheelCollider.gameObject.SetActive(false);
            backRightWheelCollider.gameObject.SetActive(false);
            backLeftWheelCollider.gameObject.SetActive(false);

            if (!isAI) 
            {
                camScript.target = explodeTarget.transform;
                camScript.height = 5;
                camScript.distance = 15;
                camScript.setBreakTime();
                if (camValue == 2) camScript.BreakGlass(true);
            }

            //Debug.Log(this.gameObject.name + "- CRASHED: " + crashed);
            //if (!crashed) Debug.Log(this.gameObject.name + " killed by " + tagTeamID);

            if (useHeadlights)
            {
                foreach (GameObject k in headlights)
                {
                    k.SetActive(false);
                }
            }
        }
    }


    void Respawn()
    {

        fireParticle.Stop();
        explodeParticle.Stop();
        fireLight.SetActive(false);
        if (GameManager.gameManager.gameState == GameManager.GameState.singleRace) currentGear = 2;
        else currentGear = 1;

        explodedModel.SetActive(false);
        wingsAnimator.SetTrigger("Respawn");
        physicsAudioSource.clip = respawnClip;
        physicsAudioSource.Play();

        rb.drag = 0.01f;
        rb.angularDrag = 0.1f;
        EventManager.eventManager.RespawnCar(this.gameObject, playerID);
        frontRightWheelCollider.gameObject.SetActive(true);
        frontLeftWheelCollider.gameObject.SetActive(true);
        backRightWheelCollider.gameObject.SetActive(true);
        backLeftWheelCollider.gameObject.SetActive(true);
        if (camValue == 1)

        if (!isAI) camScript.target = chaseCamTarget.transform;
        if (!isAI) camScript.height = startHeight;
        if (!isAI) camScript.distance = startDistance;
        if (!isAI && camValue == 2) camScript.BreakGlass(false);

        if (!isAI) gearText.text = currentGear.ToString();

        if (!isAI && useHeadlights)
        {
            foreach (GameObject h in headlights)
            {
                h.SetActive(true);
            }
        }

        rb.velocity = new Vector3(0,0,0);//Setting velocity to 0 before it gets set to race momentum the next frame is necessary to prevent car from being given momentum in the wrong direction.
        exploded = false;
    }


    void OnCollisionEnter(Collision c)
    {
        //Debug.Log(c.impulse);
        //Debug.Log(c.gameObject);
        float hiVal = Mathf.Abs(c.impulse.x);
        if (Mathf.Abs(c.impulse.y) >= hiVal) hiVal = Mathf.Abs(c.impulse.y);
        if (Mathf.Abs(c.impulse.z) >= hiVal) hiVal = Mathf.Abs(c.impulse.z);
        //Debug.Log(hiVal);
        
        if (c.collider.gameObject.layer == LayerMask.NameToLayer("EnvFloor"))
        {
            canExplode = true;
        }
        if (!invincible && hiVal >= physicsForceTolerance)
        {
            Debug.Log("ForceTolerance exceeded by " + (hiVal - physicsForceTolerance));
            Explode(true);
        }

        int hiInt = Mathf.RoundToInt(hiVal);
        if ((c.collider.gameObject.layer == LayerMask.NameToLayer("EnvFloor") || c.collider.gameObject.layer == LayerMask.NameToLayer("EnvWall")) && hiInt > 2000)//if we bump a wall
        {
            int randVal = Random.Range(0, physicsClips.Length);
            physicsAudioSource.volume = 1;
            physicsAudioSource.clip = physicsClips[randVal];
            physicsAudioSource.Play();
        }
        else if (c.collider.gameObject.layer == LayerMask.NameToLayer("CarToCar"))//if we bump another car
        {
            physicsAudioSource.volume = 0.5f;//volume gets set to half because both cars will play the sound
            physicsAudioSource.clip = bumpClip;
            physicsAudioSource.Play();
        }
    }
    void OnCollisionExit(Collision col)
    {
        if (!isAI && col.gameObject.layer == LayerMask.NameToLayer("EnvFloor"))
        {
            canExplode = false;
        }
    }


    void ShiftUp()
    {
        if (currentGear != gearVelocities.Length-1)
        {
            currentGear++;
            gearText.text = currentGear.ToString();
        }
    }
    void ShiftDown()
    {
        if (currentGear > 0)
        {
            currentGear--;
            if (currentGear == 0) gearText.text = "R";
            else gearText.text = currentGear.ToString();
        }
    }


    //void DifficultyCycle()
    //{
    //    if (handlingModel == HandlingModel.Extreme)
    //    {
    //        handlingModel = HandlingModel.Beginner;
    //        minGripAngle = 0.7f;
    //        difficultyContainerAnimator.SetTrigger("BeginnerFlash");
    //    }
    //    else
    //    {
    //        handlingModel += 1;
    //        if (handlingModel == HandlingModel.Advanced)
    //        {
    //            handlingModel = HandlingModel.Advanced;
    //            minGripAngle = 0.75f;
    //            difficultyContainerAnimator.SetTrigger("AdvancedFlash");
    //        }
    //        else if (handlingModel == HandlingModel.Expert)
    //        {
    //            handlingModel = HandlingModel.Expert;
    //            minGripAngle = 0.8f;
    //            difficultyContainerAnimator.SetTrigger("ExpertFlash");
    //        }
    //        else if (handlingModel == HandlingModel.Extreme)
    //        {
    //            handlingModel = HandlingModel.Extreme;
    //            minGripAngle = 0.8f;
    //            difficultyContainerAnimator.SetTrigger("ExtremeFlash");
    //        }
    //    }
    //}

    void AirborneStep()
    {
        if (airborneStepComplete || handlingModel == HandlingModel.Extreme) return;
        launchRot = this.gameObject.transform.rotation.y;//sets launchRot float to transform.rot.y to be used in FixedUpdate in-air assist
        airborneStepComplete = true;
    }

    IEnumerator DroneCheck()
    {
        yield return new WaitForEndOfFrame();
        if (isAI) DroneSetup(); //Must wait one frame before checking if we're an AI car because it gets set after Awake().
        else PlayerSetup();
    }

    void PlayerSetup()
    {
        Im = GetComponent<InputManager>();

        currentLapText.gameObject.SetActive(false);
        debugUiObject.gameObject.SetActive(false);
        bestLapText.gameObject.SetActive(false);

        gearText.text = currentGear.ToString();
        startDistance = camScript.distance;
        startHeight = camScript.height;

        bodyMeshRenderer.material = skins[GameManager.gameManager.player1selectedSkinID-1];
        if (EventManager.eventManager.nightTrack)
        {
            useHeadlights = true;
            bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitIdleTex);//turn on light emit texture on awake if the track is night.
        }
        else foreach (GameObject k in headlights) {k.SetActive(false);}//turn off spotlights during the day
    }

    void DroneSetup()
    {
        Destroy(GetComponent<InputManager>());
        bodyMeshRenderer.material = skins[Random.Range(0,skins.Length)];//Give AI a random skin
        bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitIdleTex);//TODO: THIS IS REDUNDANT (is also in awake()) BUT AWAKE ISN'T WORKING FOR AI CARS! WHY!?
        engineAudioSource.volume = 0f;//TODO: REDUNDANT?

        foreach (GameObject k in headlights)
        {
            k.SetActive(false);
        }

        if (EventManager.eventManager.nightTrack)
        {
            useHeadlights = true;
            bodyMeshRenderer.material.SetTexture("_EmissionMap", nightEmitIdleTex);//turn on light emit texture on awake if the track is night.
        }
    }


    public void CycleCamera()
    {
        if (lookBack) return;

        if (camValue == 1)//1=chase cam,2=hood cam
        {
            camValue = 2;
            camScript.customEnabledBool = false;
            camScript.gameObject.transform.position = hoodCamTarget.transform.position;
            camScript.gameObject.transform.rotation = hoodCamTarget.transform.rotation;
            camScript.gameObject.transform.SetParent(hoodCamTarget.transform);
            camScript.gameObject.GetComponent<Camera>().fieldOfView = 70;
            if (exploded) camScript.BreakGlass(true);
            if (!dontHideWheelMeshes)
            {
                foreach (GameObject z in steeringWheelMeshes) z.SetActive(false);
                foreach (GameObject x in throttleWheelMeshes) x.SetActive(false);
            }
        }
        else if (camValue == 2)
        {
            camValue = 1;
            camScript.customEnabledBool = true;
            camScript.gameObject.transform.SetParent(null);
            //camScript.target = chaseCamTarget.transform; TODO: ENABLE THIS WHEN MORE THAN TWO CAM POSITIONS ARE USED
            camScript.gameObject.GetComponent<Camera>().fieldOfView = 43;
            camScript.BreakGlass(false);
            if (!dontHideWheelMeshes && !exploded)
            {
                foreach (GameObject z in steeringWheelMeshes) z.SetActive(true);
                foreach (GameObject x in throttleWheelMeshes) x.SetActive(true);
            }
        }
    }


    IEnumerator SnapCamRotation()
    {
        camScript.rotationDamping = 500;
        yield return new WaitForSeconds(0.2f);
        camScript.rotationDamping = 3;
    }

    public void Lap(bool timerState, bool firstLap)
    {
        //Debug.Log("currentLap: " + currentLapElapsedTime);
        currentLapText.gameObject.SetActive(timerState);
        if (timerState) timerRunning = true;
        else timerRunning = false;
        if (!firstLap && currentLapElapsedTime < bestLapTime)
        {
            //Debug.Log("newBestLap: " + bestLapTime);
            bestLapTime = currentLapElapsedTime;
            bestLapText.gameObject.SetActive(true);
            bestLapText.text = bestLapTime.ToString();//TODO: TIMER TEXT INSTEAD OF SECONDS
        }
        currentLapElapsedTime = 0;
        currentLapStartTime = Time.time;
        if (GameManager.gameManager.gameState == GameManager.GameState.practice) lapText.text = "LAP: " + EventManager.eventManager.playerCurrentLaps[playerID-1].ToString();
        else if (GameManager.gameManager.gameState == GameManager.GameState.singleRace) lapText.text = "LAP: " + EventManager.eventManager.playerCurrentLaps[playerID-1].ToString() + "/" + GameManager.gameManager.selectedMaxLaps;
    }



    void DebugUIToggle()
    {
        if (debugUI == false)
        {
            debugUI = true;
            debugUiObject.SetActive(true);
        }
        else
        {
            debugUI = false;
            debugUiObject.SetActive(false);
        }
    }

    public void UpdateDebugUI_CheckpointHit(int cpIdReceived)
    {
        if (!debugUI) return;
        debugCheckpointHitText.text = cpIdReceived.ToString();
    }
    public void UpdateDebugUI_CheckpointCurrent(int cpIdReceived)
    {
        //Debug.LogError("fire");
        if (!debugUI) return;
        debugCheckpointCurrentText.text = (cpIdReceived.ToString() + "/" + EventManager.eventManager.maxCheckpointValue);
    }

    public void RespawnVelocity(float speed)
    {
        //TODO: alternate function for ressurect in place cheat in EventManager Respawn()
        StartCoroutine(RespawnRoutine(speed));
    }

    IEnumerator RespawnRoutine(float spawnVelocity)
    {//TODO: This section bugs when car explodes before routine is finished.
    yield return new WaitForEndOfFrame();
        bodyModel.SetActive(false);
        foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
        foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
        carToCarColObject.SetActive(false);
        yield return new WaitForEndOfFrame();
        rb.AddRelativeForce(0,0,spawnVelocity, ForceMode.VelocityChange);
        if (GameManager.gameManager.gameState == GameManager.GameState.stunt)//skip blinking if we're in stunt mode.
        {
            bodyModel.SetActive(true);
            carToCarColObject.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            //ield return null;
        }
        //////Respawn animation///// //Respawn anim is time based, not an animator, so it is consistent even at low framerates.
        else
        {
            yield return new WaitForSeconds(0.2f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);//TODO: Clean this up using a for loop.
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.19f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.18f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.17f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.16f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.15f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.14f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.13f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.12f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.11f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.1f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.09f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.08f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.07f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.06f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.05f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.04f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.03f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(false);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
            }
            yield return new WaitForSeconds(0.02f);
            bodyModel.SetActive(true);
            if (camValue == 1)
            {
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }

            //final repeated section
            for (int s = 1; s < 35; s++)
            {
                yield return new WaitForSeconds(0.02f);
                bodyModel.SetActive(false);
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(false);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(false);
                yield return new WaitForSeconds(0.01f);
                bodyModel.SetActive(true);
                foreach(GameObject n in throttleWheelMeshes) n.SetActive(true);
                foreach(GameObject n in steeringWheelMeshes) n.SetActive(true);
            }
            carToCarColObject.SetActive(true);
        }
    }

}
