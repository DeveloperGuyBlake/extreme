﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class EventManager : MonoBehaviour
{
    public static EventManager eventManager;

    [Tooltip("practice=0, singleRace=1, circuitRace=2, stunt=3, battle=4, obstacle=5, tournamentRace=6")]
    public int editorGameState = 0;

    public CarController[] carControllers;

    public Transform[] initialSpawnPoints;
    public Transform[] practiceRespawnPoints;
    public Transform[] stuntRespawnPoints;
    public Checkpoint[] checkpoints;//only used for respawning at checkpoints in singleRace currently.
    [Tooltip("only used for spawning random cars. TODO: FIX, REDUNDANT WITH GAMEMANAGER PREFABS.")]
    public GameObject[] carPrefabs;//only used for spawning random drones. TODO: FIX, REDUNDANT WITH GAMEMANAGER PREFABS.
    [Tooltip("The value of the first checkpoint. This is always 1 except when start areas are seperate from the main route.")]
    public int startCheckpointValue = -100;
    [Tooltip("The value of the final checkpoint before the finish line. Used to determine when a lap is finished.")]
    public int maxCheckpointValue = -100;
    public bool nightTrack = false;//Should cars use headlights?
    [Tooltip("The first chaseTarget to be assigned to drones when they spawn.")]
    public Transform initialDroneTarget;
    [Tooltip("The speed in meters/second cars will respawn in Race modes.")]
    public float raceSpawnVelocity = 30;

    public GameObject camPrefab;
    public GameObject cam1point1;
    public GameObject cam1point2;
    public GameObject cam1point3;
    public GameObject cam1point4;

    Animator countdownAnim;
    public AudioMixer mixer;
    public AudioSource announcerAudioSource;
    public AudioSource timerAudioSource;
    public AudioClip announcerCheckpointClip;
    public AudioClip threeClip;
    public AudioClip twoClip;
    public AudioClip oneClip;
    public AudioClip rushClip;

    public bool awaitingCountdown = true; //true if the game hasn't started yet (waiting for start countdown to finish);
    public bool gameOver = false;
    public bool paused = false;//is the game currently paused?
    
    bool startFromEditor = false;
    public GameObject defaultCarPrefab;

    int nextFinishPos = 1;//the result position the next car to cross the finish line will be given. (1st, 2nd, 3rd, etc.)
    int[] raceResults;//the order that cars crossed the finish line for results display screen. TODO: RESULTS DISPLAY SCREEN AFTER RACE (start of attract mode)!

    public int[] playerCPs;
    public int[] playerCurrentLaps;

    public AudioSource musicSource;
    public AudioSource[] raceStartAudio;//audio sources that will play once when the race starts.
    public GameObject[] raceStartObjects;//gameObjects that will be set active when the race starts (animated objects, usually).

    float gameOverWaitDuration = 5;
    float gameOverStartTime;
    float mvol = 1;
    float musicFadeStartTime;
    bool gameOverTimeSet = false;

    GameObject cam;
    SmoothFollow camScript;
    

    void Awake()
    {
        carControllers = new CarController[256];
        playerCPs = new int[256];
        playerCurrentLaps = new int[256];
        raceResults = new int[256];

        eventManager = this;

        if (GameManager.gameManager == null)
        {
            startFromEditor = true;
            GameObject g = new GameObject();
            g.AddComponent<GameManager>();//GameManager should never be null, but this is useful for testing directly in a scene without having to go through the menu first.
            if (editorGameState == 0) GameManager.gameManager.gameState = GameManager.GameState.practice;
            else if (editorGameState == 1) GameManager.gameManager.gameState = GameManager.GameState.singleRace;
            else if (editorGameState == 2) GameManager.gameManager.gameState = GameManager.GameState.circuitRace;
            else if (editorGameState == 3) GameManager.gameManager.gameState = GameManager.GameState.stunt;
            else if (editorGameState == 4) GameManager.gameManager.gameState = GameManager.GameState.battle;
            else if (editorGameState == 5) GameManager.gameManager.gameState = GameManager.GameState.obstacle;
            else if (editorGameState == 6) GameManager.gameManager.gameState = GameManager.GameState.tournamentRace;

            Debug.Log("EventManager: GAMEMANAGER DID NOT EXIST AND WAS CREATED, EVENT STARTED IN SELECTED STATE WITH DEFAULT VALUES.");
        }
        if (initialSpawnPoints.Length == 0) Debug.LogError("EVENT MANAGER SpawnPoints HAS NOT BEEN SET!");
        if (GameManager.gameManager.gameState == GameManager.GameState.practice && practiceRespawnPoints.Length == 0) Debug.LogError("EVENT MANAGER PracticeRespawnPoints HAS NOT BEEN SET!");
        if ((GameManager.gameManager.gameState == GameManager.GameState.singleRace || GameManager.gameManager.gameState == GameManager.GameState.practice) && startCheckpointValue == -100) Debug.LogError("EVENT MANAGER startCheckpointValue HAS NOT BEEN SET!");
        if ((GameManager.gameManager.gameState == GameManager.GameState.singleRace || GameManager.gameManager.gameState == GameManager.GameState.practice) && maxCheckpointValue == -100) Debug.LogError("EVENT MANAGER maxCheckpointValue HAS NOT BEEN SET!");

        foreach (int i in playerCPs)
        {
            playerCPs[i] = startCheckpointValue - 1;
        }
    }

    IEnumerator Start()
    {
        GameObject c1;
        if (!startFromEditor) c1 = Instantiate(GameManager.gameManager.vehicleList[GameManager.gameManager.player1selectedCarID], initialSpawnPoints[0].transform.position, initialSpawnPoints[0].transform.rotation);
        else c1 = Instantiate(defaultCarPrefab, initialSpawnPoints[0].transform.position, initialSpawnPoints[0].transform.rotation);
        carControllers[0] = c1.GetComponent<CarController>();
        cam = Instantiate(camPrefab, c1.GetComponent<CarController>().chaseCamTarget.transform);
        cam.GetComponent<CamAnimSequence>().cc = carControllers[0];
        SmoothFollow c2 = cam.GetComponent<SmoothFollow>();
        camScript = c2;//Sets up local camScript when spawning the player car
        c2.target = c1.GetComponent<CarController>().chaseCamTarget.transform;
        cam.GetComponent<CamAnimSequence>().cc = carControllers[0];
        countdownAnim = cam.GetComponent<Animator>();
        carControllers[0].camScript = c2;
        carControllers[0].gearText = GameObject.Find("GearText").GetComponent<Text>();//...gameObject.GetComponent<Text>()?
        carControllers[0].currentLapText = GameObject.Find("CurrentLapTimeText").GetComponent<Text>();
        carControllers[0].bestLapText = GameObject.Find("BestLapText").GetComponent<Text>();
        carControllers[0].debugUiObject = GameObject.Find("DEBUG").gameObject;
        carControllers[0].debugCheckpointHitText = GameObject.Find("CheckpointHitText").GetComponent<Text>();
        carControllers[0].debugCheckpointCurrentText = GameObject.Find("CheckpointCurrentText").GetComponent<Text>();
        carControllers[0].lapText = GameObject.Find("LapText").GetComponent<Text>();
        carControllers[0].playerID = 1;
        //TODO: SPAWN PLAYERS 2-4

        if (GameManager.gameManager.gameState == GameManager.GameState.practice)//PRACTICE MODE
        {
            //No drones are spawned in practice mode.
        }
        else if (GameManager.gameManager.gameState == GameManager.GameState.stunt)//STUNT MODE
        {
            //TODO: Add drone spawning in stunt mode.
        }
        else if (GameManager.gameManager.gameState == GameManager.GameState.singleRace)//SINGLE RACE MODE
        {
            for (int i = GameManager.gameManager.playerCount+1; i <= GameManager.gameManager.racerCount; i++)
            {
                int randInt = Random.Range(0, carPrefabs.Length);
                CarController c = GameObject.Instantiate(carPrefabs[randInt], initialSpawnPoints[i-1]).GetComponent<CarController>();
                c.isAI = true;
                DroneScript ds = c.gameObject.AddComponent<DroneScript>();
                ds.chaseTarget = initialDroneTarget;
                c.ds = ds;

                carControllers[i-1] = c;
                c.playerID = i;
                yield return new WaitForEndOfFrame();
            }
        }

        StartCoroutine(StartCountdown());
    }

    void Update()
    {
        if ( !gameOver && !awaitingCountdown && Input.GetButtonDown("Pause1"))//Pause
        {
            TogglePause();
        }
        if (gameOver && !gameOverTimeSet)
        {
            gameOverStartTime = Time.time;
            gameOverTimeSet = true;
        }
        if (gameOver)
        {
            mvol = ((Time.time - musicFadeStartTime) * -0.25f) + 1;
            musicSource.volume = mvol;
            Mathf.Clamp(musicSource.volume,0,1);
            //Debug.Log("mvol: " + mvol.ToString());
        }
        if (gameOver && Time.time >= gameOverStartTime + gameOverWaitDuration) SceneManager.LoadScene(0);

        /////PAUSE MENU NAVIGATION/////

    }

    public void RespawnCar(GameObject c, int respawnIDreceived)
    {
        if (GameManager.gameManager.permaDeath) return;
        ////////// PRACTICE MODE //////////
        if (GameManager.gameManager.gameState == GameManager.GameState.practice)//Practice mode respawn
        {
            Transform closestSpawnPoint = null;
            float closestDistanceSqr = Mathf.Infinity;
            foreach(Transform point in practiceRespawnPoints)//checks distance to all practiceRespawnPoints to find the closest one.
            {
                Vector3 directionToTarget = point.position - c.transform.position;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if(dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    closestSpawnPoint = point;
                }
            }
            //Debug.Log(closestSpawnPoint.gameObject.name);
            c.transform.position = closestSpawnPoint.transform.position;
            c.transform.rotation = closestSpawnPoint.transform.rotation;
            c.GetComponent<CarController>().RespawnVelocity(0);
        }
        ////////// STUNT MODE //////////
        if (GameManager.gameManager.gameState == GameManager.GameState.stunt)//Practice mode respawn
        {
            Transform closestSpawnPoint = null;
            float closestDistanceSqr = Mathf.Infinity;
            foreach(Transform point in stuntRespawnPoints)//checks distance to all practiceRespawnPoints to find the closest one.
            {
                Vector3 directionToTarget = point.position - c.transform.position;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if(dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    closestSpawnPoint = point;
                }
            }
            //Debug.Log(closestSpawnPoint.gameObject.name);
            c.transform.position = closestSpawnPoint.transform.position;
            c.transform.rotation = closestSpawnPoint.transform.rotation;
            c.GetComponent<CarController>().RespawnVelocity(0);
        }
        ////////// SINGLE RACE MODE //////////
        else if (GameManager.gameManager.gameState == GameManager.GameState.singleRace)//SingleRace mode respawn
        {
            if (playerCPs[respawnIDreceived-1] == maxCheckpointValue && playerCurrentLaps[respawnIDreceived] == GameManager.gameManager.selectedMaxLaps)//if the next checkpoint is the finish line and it's the final lap, put the player before the finish line.
            {
                c.transform.position = checkpoints[playerCPs[respawnIDreceived-1]-1].transform.position;
                c.transform.rotation = checkpoints[playerCPs[respawnIDreceived-1]-1].transform.rotation;
            }
            else if (playerCPs[respawnIDreceived-1] == 0)//if the player crashes before reaching the first checkpoint
            {
                c.transform.position = checkpoints[0].transform.position;
                c.transform.rotation = checkpoints[0].transform.rotation;
            }
            else
            {
                Checkpoint nextCP = checkpoints[playerCPs[respawnIDreceived-1]-1].nextCheckpoint;
                //Debug.Log("Player " + respawnIDreceived + "'s nextCP is " + nextCP.gameObject.name);
                if (nextCP.overrideSpawnPos)//if the checkpoint has overrideSpawnPos set, spawn the car at the override position instead.
                {
                    c.transform.position = nextCP.overridePos.gameObject.transform.position;
                    c.transform.rotation = nextCP.overridePos.gameObject.transform.rotation;
                }
                else
                {
                    c.transform.position = nextCP.gameObject.transform.position;
                    c.transform.rotation = nextCP.gameObject.transform.rotation;
                }
            }
            c.GetComponent<CarController>().RespawnVelocity(raceSpawnVelocity);//Gives cars momentum when respawning in Race modes.
        }
    }

    public void Checkpoint(int playerIDreceived, int checkpointIDreceived, bool bonusTime, bool isAI)
    {
        if (GameManager.gameManager.gameState == GameManager.GameState.stunt || GameManager.gameManager.gameState == GameManager.GameState.battle) return;
        if (!carControllers[playerIDreceived-1].isAI) carControllers[playerIDreceived-1].UpdateDebugUI_CheckpointHit(checkpointIDreceived);
        if (playerCurrentLaps[playerIDreceived-1] == 0 && checkpointIDreceived == startCheckpointValue)             //If cross start line on lap 1
        {
            playerCurrentLaps[playerIDreceived-1] = 1;
            if (!carControllers[playerIDreceived-1].isAI)carControllers[playerIDreceived-1].Lap(true, true);
        }
        if (checkpointIDreceived == playerCPs[playerIDreceived-1] + 1)//If progressed to next checkpoint
        {
            playerCPs[playerIDreceived-1] = checkpointIDreceived;//TODO: ADD SUPPORT FOR RACING IN REVERSE DIRECTION.
            if (!carControllers[playerIDreceived-1].isAI) carControllers[playerIDreceived-1].UpdateDebugUI_CheckpointCurrent(checkpointIDreceived);
            if (bonusTime && playerIDreceived == 1)//TODO: ADD TIME SUPPORT FOR PLAYERS 2-4
            {
                //TODO: ADD BONUS TIME
                announcerAudioSource.clip = announcerCheckpointClip;
                announcerAudioSource.Play();
                timerAudioSource.Play();
            }
        }
        if (playerCPs[playerIDreceived-1] == maxCheckpointValue && checkpointIDreceived == 1)//If cross finish line
        {
            if (GameManager.gameManager.gameState == GameManager.GameState.practice)//practice mode
            {
                playerCPs[playerIDreceived-1] = 1;
                playerCurrentLaps[playerIDreceived-1] += 1;
                if (!carControllers[playerIDreceived-1].isAI) carControllers[playerIDreceived-1].Lap(true, false);
            }
            else//not practice mode
            {
                if (playerCurrentLaps[playerIDreceived-1] < GameManager.gameManager.selectedMaxLaps)
                {
                    playerCPs[playerIDreceived-1] = 1;
                    playerCurrentLaps[playerIDreceived-1] += 1;
                    //TODO: Update current lap display in player UI
                    if (!carControllers[playerIDreceived-1].isAI) carControllers[playerIDreceived-1].Lap(true, false);
                }
                else//if final lap, end the race.
                {
                    if (!carControllers[playerIDreceived-1].isAI)//end the game when the player finishes the race
                    {
                        gameOver = true;
                        musicFadeStartTime = Time.time;
                    }
                    else carControllers[playerIDreceived-1].ds.finishedRace = true;
                    if (!carControllers[playerIDreceived-1].isAI) carControllers[playerIDreceived-1].Lap(false, false);
                    
                    raceResults[nextFinishPos-1] = nextFinishPos;
                    if (!isAI)
                    {
                        bool lp = false;
                        if (nextFinishPos == GameManager.gameManager.racerCount) lp = true;
                        carControllers[playerIDreceived-1].camScript.FinishLineDisplay(nextFinishPos, lp);//if this player was the last to cross the line, tells the camera to display LastPlace instead of finishing pos.
                        //TODO: populate the remaining slots for number of drones that haven't finished with their current positions.
                    }
                    nextFinishPos += 1;
                    
                    //TODO: RESULTS SCREEN AFTER RACE (transition to Attract mode)
                }
            }
        }
    }

    IEnumerator StartCountdown()
    {
        awaitingCountdown = true;
        countdownAnim.SetTrigger("Three");
        announcerAudioSource.clip = threeClip;
        announcerAudioSource.Play();
        yield return new WaitForSeconds(1f);
        countdownAnim.SetTrigger("Two");
        announcerAudioSource.clip = twoClip;
        announcerAudioSource.Play();
        yield return new WaitForSeconds(1f);
        countdownAnim.SetTrigger("One");
        announcerAudioSource.clip = oneClip;
        announcerAudioSource.Play();
        yield return new WaitForSeconds(1f);
        countdownAnim.SetTrigger("Idle");
        announcerAudioSource.clip = rushClip;
        announcerAudioSource.Play();

        awaitingCountdown = false;
        StartRaceProcess();
    }

    void StartRaceProcess()
    {
        foreach (AudioSource s in raceStartAudio) s.Play();
        foreach (GameObject g in raceStartObjects) g.SetActive(true);
    }

    public void TogglePause()
    {
        if (!paused)//Pause game
        {
            paused = true;
            Time.timeScale = 0;
            mixer.SetFloat("EnvSFX", -80);
            mixer.SetFloat("CarSFX", -80);
            mixer.SetFloat("CarEngine", -80);
            mixer.SetFloat("CarTires", -80);
            musicSource.Pause();
            camScript.Pause(true);
        }
        else//Unpause game
        {
            paused = false;
            Time.timeScale = 1;
            //set audio mixer values (and set up menu sfx source)
            musicSource.UnPause();
            camScript.Pause(false);
        }
    }
}
